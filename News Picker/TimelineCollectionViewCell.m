//
//  TimelineCollectionViewCell.m
//  News Picker
//
//  Created by THENOT Geoffrey on 26/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "TimelineCollectionViewCell.h"

@implementation TimelineCollectionViewCell


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        UIView *aview = [[UIView alloc]initWithFrame:self.bounds];

        aview.backgroundColor = [UIColor redColor];
        
        [self.contentView addSubview:aview];
    }
    
    return self;
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    

}

@end
