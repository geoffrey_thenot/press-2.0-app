//
//  ArticleDetailList.m
//  News Picker
//
//  Created by Guillaume Jasmin on 31/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "ArticleDetailList.h"
#import "ManagerData.h"

@interface ArticleDetailList ()

@property (strong, nonatomic) IBOutlet UIScrollView *horizontalScrollView;

@property (strong, nonatomic) NSMutableArray *articlesDetailList;
@property (strong, nonatomic) NSMutableArray *viewControllers;
@property (strong, nonatomic) NSArray *articles;
@property (strong, nonatomic) ManagerData *manager;
@property (assign, nonatomic) NSInteger goToIndex;


@end

@implementation ArticleDetailList

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // ...
    }
    return self;
}

- (void)viewDidLoad
{
    
    // [self initArticlesDetailList];
    // [self initArticlesDetailListWithArticle:nil];
}

- (void)initArticlesDetailListWithArticles:(NSArray *)articles goToIndex:(NSInteger)goToIndex {
    
    // self.article = article;
    self.articles = articles;
    self.goToIndex = goToIndex;
    
    self.horizontalScrollView.delegate = self;
    
    self.manager = [ManagerData sharedManager];
    
    self.manager.articleDetailList = self;
    
    // self.articlesDetailList = [[NSMutableArray alloc] initWithObjects:@"one", @"two",nil];
    
    self.articlesDetailList = [[NSMutableArray alloc] initWithArray:self.articles];
    
    NSUInteger numberPages = [self.articlesDetailList count];
    
    // view controllers are created lazily
    // in the meantime, load the array with placeholders which will be replaced on demand
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < numberPages; i++) {
        [controllers addObject:[NSNull null]];
    }
    
    self.viewControllers = controllers;
    
    // a page is the width of the scroll view
    self.horizontalScrollView.pagingEnabled = YES;
    self.horizontalScrollView.contentSize = CGSizeMake(self.view.frame.size.width * numberPages, self.horizontalScrollView.frame.size.height);
    self.horizontalScrollView.showsHorizontalScrollIndicator = YES;
    self.horizontalScrollView.showsVerticalScrollIndicator = NO;
    self.horizontalScrollView.scrollsToTop = NO;
    self.automaticallyAdjustsScrollViewInsets = NO;
    

    
    // load last
    if(self.goToIndex > 0){
        [self loadScrollViewWithPage:self.goToIndex - 1];
    }
    
    // load next
    if (self.goToIndex < self.articles.count - 1){
        [self loadScrollViewWithPage:self.goToIndex + 1];
    }
    
    // load current
    [self loadScrollViewWithPage:self.goToIndex];
    self.currentArticleIndex = goToIndex;
    [self updateCurrentArticle];
    [self.currentArticleViewController animateFromTimeline];
    
    
    [self.horizontalScrollView layoutIfNeeded];
    
    // NSLog(@"%f", self.horizontalScrollView.frame.size.width);
    
    [self.horizontalScrollView scrollRectToVisible:CGRectMake([[UIScreen mainScreen] bounds].size.width * self.goToIndex, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height) animated:NO];
}

- (void)loadScrollViewWithPage:(NSUInteger)page
{
    if (page >= [self.articlesDetailList count]) {
        return;
    }
    
    // replace the placeholder if necessary
    ArticleViewController *controller = [self.viewControllers objectAtIndex:page];
    if ((NSNull *)controller == [NSNull null]) {
        //controller = [[Timeline alloc] init];
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Article"];
        [self.viewControllers replaceObjectAtIndex:page withObject:controller];
    }
    

    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        [self initTableViewSize:controller.view page:page];
        [self.horizontalScrollView addSubview:controller.view];
        [controller initWithArticle:self.articles[page] animated:NO];
    }
    
    
}

// at the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = CGRectGetWidth(self.horizontalScrollView.frame);
    NSUInteger page = floor((self.horizontalScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    self.currentArticleIndex = page;
    
    [self updateCurrentArticle];
    
}


- (void)initTableViewSize:(UIView *)view page:(NSInteger)page
{
    CGRect frame = self.horizontalScrollView.frame;
    frame.origin.x = CGRectGetWidth(self.view.frame) * page; // set x
    view.frame = frame;
}


- (void)animateFromTimelineWithIndex:(NSInteger)index
{
    
    self.scrollViewConstraintTop.constant = self.scrollViewConstraintTopCloseState;
    self.scrollViewConstraintBottom.constant = self.scrollViewConstraintBottomCloseState;
    
    [self.view layoutIfNeeded];
    
    self.scrollViewConstraintTopOpenState = 0;
    self.scrollViewConstraintBottomOpenState = 0;
    
    self.scrollViewConstraintTop.constant = self.scrollViewConstraintTopOpenState;
    self.scrollViewConstraintBottom.constant = self.scrollViewConstraintBottomOpenState;
    
    // self.iconHighlightedConstraintTop.constant = 30;
    
//    if(self.articleDetailList == nil){
//        self.articleDetailList = [self.storyboard instantiateViewControllerWithIdentifier:@"ArticlesDetailListXib"];
//    }
    
    // rotate button
    [self.currentArticleViewController rotateCloseBtn:CloseBtnStateToOpen animated:YES];
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionNone | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                         
                         self.scrollViewConstraintTop.constant = 0;
                         self.scrollViewConstraintBottom.constant = 0;
                         
                         [self.view layoutIfNeeded];
                         
                         self.manager.currentTimeline.currentTimelineItem.isOpeningToArticleDetail = NO;
                         
                         // [self.view removeFromSuperview];
                         
                         [self.manager.mainViewController.navigationController pushViewController:self animated:NO];
                         
                         // close timeline item
                         [self.manager.currentTimeline closeArticleAtIndex:index];
                     }];
}


- (void)closeArticle
{
        // scroll top
        [self.currentArticleViewController.scrollView setContentOffset:CGPointZero animated:YES];
    
    
        // scroll to article in timeline
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.currentArticleIndex inSection:0];
        UITableViewScrollPosition scrollPosition;
    
        if (self.currentArticleIndex < 2) {
            scrollPosition = UITableViewScrollPositionTop;
            self.scrollViewConstraintTopCloseState = 64;
            self.scrollViewConstraintBottomCloseState = ([[UIScreen mainScreen] bounds].size.height - 64 - 320);
        }
        else if(self.currentArticleIndex >= self.viewControllers.count - 2){
            scrollPosition = UITableViewScrollPositionBottom;
            self.scrollViewConstraintTopCloseState = ([[UIScreen mainScreen] bounds].size.height - 320);
            self.scrollViewConstraintBottomCloseState = 0;
        }
        else {
            scrollPosition = UITableViewScrollPositionMiddle;
            self.scrollViewConstraintBottomCloseState = ([[UIScreen mainScreen] bounds].size.height - 64 - 320) / 2;
            self.scrollViewConstraintTopCloseState = self.scrollViewConstraintBottomCloseState + 64;
        }
    
        [self.manager.currentTimeline.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:NO];
        [self.manager.currentTimeline openArticleAtIndex:self.currentArticleIndex animated:NO];
        [self.manager.currentTimeline.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:scrollPosition animated:NO];
    
        [self.navigationController popViewControllerAnimated:NO];
        [self.manager.mainViewController.view addSubview:self.view];
    
        double delayInSeconds = 0.5;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self nextAnimtClose];
        });
    
    
}

- (void)nextAnimtClose
{
    // rotate button
    [self.currentArticleViewController rotateCloseBtn:CloseBtnStateToClose animated:YES];
    
    self.currentArticleViewController.articleImageFiltered.layer.mask = self.manager.currentArticle.maskLayerLarge;
    
    self.scrollViewConstraintTop.constant    = self.scrollViewConstraintTopCloseState;
    self.scrollViewConstraintBottom.constant = self.scrollViewConstraintBottomCloseState;
    
    // highlighted icon
    self.currentArticleViewController.iconHighlightedConstraintTop.constant = 10;
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionNone | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                         
                         [self.currentArticleViewController.view layoutIfNeeded];
                         
                     }
                     completion:^(BOOL finished){
                         
                         self.manager.mainViewController.currentArticleContext = CurrentArticleContextTimeline;
                         
                         self.scrollViewConstraintTop.constant    = self.scrollViewConstraintTopOpenState;
                         self.scrollViewConstraintBottom.constant = self.scrollViewConstraintBottomOpenState;
                         
                         [self.view layoutIfNeeded];
                         
                         // self.view.hidden = YES;
                         [self.view removeFromSuperview];
                         
                         for (ArticleViewController *viewController in self.viewControllers) {
                             if (![viewController isKindOfClass:[NSNull class]]){
                                 [viewController.view removeFromSuperview];
                             }
                             
                         }
                         
                         [self.manager.currentTimeline afterAnimationFromArticleDetail];
                         
                     }];
}

- (void)updateCurrentArticle
{
    self.currentArticleViewController = [self.viewControllers objectAtIndex:self.currentArticleIndex];
    // NSLog(@"-------ttt %@", self.currentArticleViewController);
    self.manager.currentArticle = self.currentArticleViewController.article;
    // NSLog(@"----- eee %@", self.manager.currentArticle.PFArticle[@"title"]);
}



@end
