//
//  PinchOutCollectionViewController.h
//  News Picker
//
//  Created by THENOT Geoffrey on 26/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PinchOutCollectionViewController : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property(nonatomic) NSArray *timelines;
@end
