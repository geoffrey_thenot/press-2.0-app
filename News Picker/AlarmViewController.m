//
//  AlarmViewController.m
//  News Picker
//
//  Created by THENOT Geoffrey on 24/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "AlarmViewController.h"
#import "Helper.h"
#import "NPAlarm.h"
#import <CoreData/CoreData.h>
#import "AppDelegate.h"

@interface AlarmViewController ()
@property (nonatomic,strong) NSDateFormatter *dateFormatter;
@property (nonatomic,strong) NSCalendar *calendar;
@property (weak, nonatomic) IBOutlet UISwitch *alarmSwitch;
@property (nonatomic,strong) NSDate *date;
@property (strong, nonatomic) IBOutlet UIImageView *clockImageView;

@property (nonatomic, strong) NSManagedObject *alarm;
@end

@implementation AlarmViewController {
    NSFetchedResultsController* fetchedResultsController;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.myClock1 stopRealTime];
    self.myClock1.enableShadows = YES;
    self.myClock1.realTime = NO;
    self.myClock1.currentTime = NO;
    self.myClock1.setTimeViaTouch = YES;
    self.myClock1.borderColor = [UIColor clearColor];
    self.myClock1.borderWidth = 3;
    self.myClock1.faceBackgroundColor = [UIColor whiteColor];
    self.myClock1.faceBackgroundAlpha = 0.0;
    self.myClock1.delegate = self;
    self.myClock1.digitFont = [UIFont fontWithName:@"OpenSans-Light" size:17];
    self.myClock1.digitColor = [UIColor NPLightGray];
    self.myClock1.enableDigit = YES;
    self.myClock1.minuteHandColor = [UIColor NPLightGray];
    self.myClock1.hourHandColor = [UIColor NPLightGray];
    self.myClock1.delegate = self;
    self.myClock1.militaryTime = YES;
    [self initAlarm];
}

- (void)currentTimeOnClock:(BEMAnalogClockView *)clock Hours:(NSString *)hours Minutes:(NSString *)minutes Seconds:(NSString *)seconds {
    int hoursInt = [hours intValue];
    int minutesInt = [minutes intValue];
    self.dateLabel = [NSString stringWithFormat:@"%02d:%02d", hoursInt, minutesInt];
    [self.delegate alarmDidChange:self.dateLabel];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    self.date = [calendar dateBySettingHour:(hoursInt) minute:minutesInt second:1 ofDate:[NSDate date] options:0];
    NSLog(@"change hours");
}


- (IBAction)switchAlarm:(id)sender {
    if([sender isOn]){
        self.myClock1.digitColor = [UIColor NPDarkGray];
        [self updateClockColor:[UIColor NPYellow]];
        self.clockImageView.image = [UIImage imageNamed:@"clock-active"];
    } else{
        self.myClock1.digitColor = [UIColor NPLightGray];
        [self updateClockColor:[UIColor NPLightGray]];
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        self.clockImageView.image = [UIImage imageNamed:@"clock"];
    }
    [self saveAlarm];
}

-(void)updateClockColor:(UIColor *)color {
    self.myClock1.minuteHandColor = color;
    self.myClock1.hourHandColor = color;
    [self.myClock1 reloadClock];
}

-(void)saveAlarm {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSManagedObject *newAlarm = [NSEntityDescription
                                 insertNewObjectForEntityForName:@"Alarm"
                                 inManagedObjectContext:context];
    
    //Here we need to get rid of the date portion of the date
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comps = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond) fromDate:_date];
    NSDate *timeWithoutDate = [calendar dateFromComponents:comps];
    
    [newAlarm setValue:timeWithoutDate forKey:@"date"];
    [newAlarm setValue:[NSDate date] forKey:@"created"];
    [newAlarm setValue:[NSNumber numberWithBool:self.alarmSwitch.isOn] forKey:@"isActive"];
    NSError *error;
    [context save:&error];

    [self scheduleLocalNotification];
    
    [[self presentingViewController] dismissViewControllerAnimated:YES completion:nil];
}


- (void)scheduleLocalNotification {
    if (self.alarmSwitch.isOn) {
        // We need to schedule a local notification
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        localNotification.fireDate = _date;
        localNotification.alertBody = @"Wake up !";
        localNotification.timeZone = [NSTimeZone systemTimeZone];
        localNotification.alertAction = @"I'm up";
        localNotification.repeatInterval = NSCalendarUnitDay;
        localNotification.soundName = @"alarm.mp3";
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    }

}

-(void)initAlarm {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    fetchedResultsController = [[NSFetchedResultsController alloc]
                                initWithFetchRequest:fetchRequest
                                managedObjectContext:context
                                sectionNameKeyPath:nil
                                cacheName:nil];
    
    
    NSError *error;
    BOOL success = [fetchedResultsController performFetch:&error];
    if(!success) {
        NSLog(@"There was ab error fetching data: %@", error);
    }
    NSLog(@"INIT ALARM : %li", [[fetchedResultsController fetchedObjects] count]);
    if ( [[fetchedResultsController fetchedObjects] count] > 0) {
        self.alarm = [[fetchedResultsController fetchedObjects] lastObject];
        
        self.date = [self.alarm valueForKey:@"date"];
        NSLog(@"date: %@", [self.alarm valueForKey:@"date"]);
        if (self.date == nil) {
            self.date = [NSDate date];
        }
        [self.alarmSwitch setOn:[(NSNumber*)[self.alarm valueForKey:@"isActive"] boolValue]];
        if (self.alarmSwitch.isOn) {
            self.myClock1.digitColor = [UIColor NPDarkGray];
            [self updateClockColor:[UIColor NPYellow]];
            self.clockImageView.image = [UIImage imageNamed:@"clock-active"];
        }
    } else {
        self.date = [NSDate date];
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *date = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:self.date];
    self.myClock1.hours = date.hour;
    self.myClock1.minutes = date.minute;
    self.myClock1.seconds = date.second;
    [self.myClock1 updateTimeAnimated:NO];
    
    self.dateLabel = [NSString stringWithFormat:@"%02ld:%02ld", (long)date.hour, (long)date.minute];
    [self.delegate alarmDidChange:self.dateLabel];
    NSLog(@"init alarm count: %li", [[fetchedResultsController fetchedObjects] count]);
}

-(void)touchEnded {
    [self saveAlarm];
}

@end
