//
//  Timeline.m
//  News Picker
//
//  Created by JASMIN Guillaume on 12/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <Parse/Parse.h>
#import "Timeline.h"
#import "TimelineItemData.h"
#import "ArticleViewController.h"
#import "AVSpeechSynthesizerController.h"
#import "ManagerData.h"
#import "Helper.h"


#define LOADER_IMAGES_COUNT 24
#define QUERY_LIMIT 10

@interface Timeline ()

@property (strong, nonatomic) ArticleViewController *articleViewController;
@property (strong, nonatomic) ManagerData           *manager;
@property (strong, nonatomic) UIView                *loaderView;
@property (strong, nonatomic) UIImageView           *spinner;
@property (strong, nonatomic) NSArray               *timelineItemDataList;
@property (strong, nonatomic) NSArray               *categories;
@property (assign, nonatomic) NSInteger             categoriesRemainingToLoad;
@property (assign, nonatomic) NSInteger             selectedNumber;
@property (assign, nonatomic) NSInteger             querySkip;
@property (assign, nonatomic) NSInteger             day;
@property (assign, nonatomic) NSInteger             month;
@property (assign, nonatomic) NSInteger             year;
@property (assign, nonatomic) BOOL                  isLoadingData;
@property (assign, nonatomic) BOOL                  allDataDayAreLoaded;

@end

@implementation Timeline

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    
    self.navigationController.navigationBarHidden = YES;
    self.clearsSelectionOnViewWillAppear = NO;
    
    self.manager = [ManagerData sharedManager];
    [self loader];

    self.selectedNumber = -1;
    self.querySkip = 0;
    self.isLoadingData = YES;
    self.allDataDayAreLoaded = NO;
    self.playInContinue = YES;
    
    //[self loadDataWithValue:10 setMonth:12 setYear:2014];
    
    [self setupRefreshControl];
}

-(void)initWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year {
        self.day   = day;
        self.month = month;
        self.year  = year;
        
        [self loadData];
}

- (void)loader {
    self.loaderView = [[UIView alloc] initWithFrame:self.view.frame];
    self.loaderView.backgroundColor = [UIColor whiteColor];
    self.loaderView.layer.zPosition = 1;
    
    UIImageView *loader = [[UIImageView alloc]
                                initWithFrame:CGRectMake(
                                    ([[UIScreen mainScreen] bounds].size.width - 50) / 2,
                                    ([[UIScreen mainScreen] bounds].size.height - 50 - 64) / 2,
                                    50,
                                    50
                                )];
    
    loader.animationImages = [Helper createAnimationArray:@"animation-loader_" imgCout:LOADER_IMAGES_COUNT];
    loader.animationDuration = 1;
    loader.animationRepeatCount = 0;
    NSString *imageName = @"animation-loader_00014";
    loader.image = [UIImage imageNamed:imageName];
    [loader startAnimating];
    
    [self.loaderView addSubview:loader];
    [self.view addSubview:self.loaderView];
}

- (void)loadData
{
    
    if (self.allDataDayAreLoaded) {
        NSLog(@"allDataDayAreLoaded");
        return;
    }
    
    NSDate *startDate;
    NSDate *endDate;
    
    if (self.day) {
        NSCalendar *cal = [NSCalendar currentCalendar];
        
        NSDateComponents *startDateComponent = [[NSDateComponents alloc] init];
        [startDateComponent setDay:self.day];
        [startDateComponent setMonth:self.month];
        [startDateComponent setYear:self.year];
        startDate = [cal dateFromComponents:startDateComponent];
        
        NSDateComponents *endDateComponent = [[NSDateComponents alloc] init];
        [endDateComponent setSecond:59];
        [endDateComponent setMinute:59];
        [endDateComponent setHour:23];
        [endDateComponent setDay:self.day];
        [endDateComponent setMonth:self.month];
        [endDateComponent setYear:self.year];
        endDate = [cal dateFromComponents:endDateComponent];
    }
    
    self.isLoadingData = YES;

    NSMutableArray *timelineItemDataListTemp = [[NSMutableArray alloc] initWithArray:self.timelineItemDataList];
    NSMutableArray *categoriesTemp           = [[NSMutableArray alloc] initWithArray:self.categories];
    
    PFQuery *query = [PFQuery queryWithClassName:@"Article"];
    query.limit = QUERY_LIMIT;
    query.skip = self.querySkip;
    [query orderByDescending:@"date"];
    [query whereKey:@"img" notEqualTo:@""];
    
    if (self.day) {
        [query whereKey:@"date" greaterThanOrEqualTo:startDate];
        [query whereKey:@"date" lessThanOrEqualTo:endDate];
    }
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            // The find succeeded.
            NSLog(@"Successfully retrieved %lu scores.", (unsigned long)objects.count);
            // Do something with the found objects
            
            if (objects.count == 0) {
                self.allDataDayAreLoaded = YES;
            }
            self.categoriesRemainingToLoad = objects.count;
            for (PFObject *object in objects) {
                
                Article *article = [[Article alloc] initWithPFArticle:object];
                
                TimelineItemData *timelineItemData = [[TimelineItemData alloc] initWithArticle:article];
                [timelineItemDataListTemp addObject:timelineItemData];
                self.timelineItemDataList = timelineItemDataListTemp;
                
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.timelineItemDataList.count - 1 inSection:0];
                [self.tableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
                PFRelation *relation = [article.PFArticle relationForKey:@"Categories"];
                
                [categoriesTemp addObject:@""];
                self.categories = categoriesTemp;
                
                [self loadCategory:relation index:self.timelineItemDataList.count - 1];
                
                if(self.timelineItemDataList.count == 1 || self.timelineItemDataList.count == 3) {
                    article.isHighlightedByPeople = YES;
                }
                
            }
            
            self.querySkip += QUERY_LIMIT;
            
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
        
    }];
}

- (void)loadCategory:(PFRelation *)relation index:(NSInteger)index
{
    
    [[relation query] findObjectsInBackgroundWithBlock:^(NSArray *categories, NSError *error) {
        if (error) {
            // There was an error
        } else {
            
            TimelineItemData *timelineItemData = self.timelineItemDataList[index];
            timelineItemData.article.category = categories[0][@"name"];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            self.categoriesRemainingToLoad -= 1;
            if(self.categoriesRemainingToLoad <= 0) {
                [self endLoading];
            }
        }
    }];
}

- (void)endLoading {
    
    self.loaderView.hidden = YES;
    NSLog(@"reload categories");
    self.isLoadingData = NO;
    
    if (self.manager.willPlayTimeline) {
        self.manager.willPlayTimeline = NO;
        
        double delayInSeconds = 1;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            [self playArticleAtIndex:0];
        });
    }
}


- (void)reloadTableCell:(NSIndexPath *)indexPath
{
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 3;
}
*/

// --------------------------------------------------------------
//                           Number of
// --------------------------------------------------------------
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.timelineItemDataList.count;
}


// --------------------------------------------------------------
//                           Render Cell
// --------------------------------------------------------------
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    // NSLog(@"render cell %ld", (long)indexPath.row);
    
    static NSString *CellIdentifier = @"TimelineItem";
    TimelineItem *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(cell == nil) {
        cell = [[TimelineItem alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }

    if(self.timelineItemDataList.count - 1 >= indexPath.row) {
        
        TimelineItemData *timelineItemData = self.timelineItemDataList[indexPath.row];
        
        [cell clean];
        [cell updateData:timelineItemData];
        
        if(cell.data.imageIsLoading != YES && cell.data.imageIsLoad != YES){
            // NSLog(@"loadOneImage %ld", (long)indexPath.row);
            [self performSelectorInBackground:@selector(loadOneImage:) withObject:indexPath];
        }
        
        // if cell is already open
        if(self.selectedNumber == indexPath.row) {
            NSLog(@"----- open !");
            [cell openWithAnimation:NO];
        }
        
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
        
    }
    else {
        NSLog(@"data not ready");
    }
    
    if(!self.isLoadingData && indexPath.row > self.timelineItemDataList.count - 4){
        NSLog(@"load more");
        [self loadData];
    }
    
    return cell;
}

- (void)loadOneImage:(NSIndexPath *)indexPath
{
    TimelineItemData *timelineItemData = self.timelineItemDataList[indexPath.row];
    timelineItemData.imageIsLoading = YES;
    
    if (timelineItemData.article.PFArticle[@"img"] == nil) {
        NSLog(@"------------------ no image");
        timelineItemData.imageIsLoad = YES;
        return;
    }
    
    NSURL *imageURL = [NSURL URLWithString:timelineItemData.article.PFArticle[@"img"]];
    timelineItemData.article.imageData = [NSData dataWithContentsOfURL:imageURL];
    UIImage *image = [UIImage imageWithData:timelineItemData.article.imageData];
    
    
    if(![[self contentTypeForImageData:timelineItemData.article.imageData] isEqualToString:@"image/jpeg"]) {
        NSLog(@"------------------ bad format");
        timelineItemData.imageIsLoad = YES;
        return;
    }

    timelineItemData.article.image = image;
    [timelineItemData.article setupImage];
    
    timelineItemData.imageIsLoad = YES;
    
    [self performSelectorOnMainThread:@selector(reloadTableCell:) withObject:indexPath waitUntilDone:YES];
}


- (NSString *)contentTypeForImageData:(NSData *)data
{
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"image/jpeg";
        case 0x89:
            return @"image/png";
        case 0x47:
            return @"image/gif";
        case 0x49:
        case 0x4D:
            return @"image/tiff";
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.selectedNumber) {
        return 320;
    }
    else {
        return 115;
    }
}


// --------------------------------------------------------------
//                           Select
// --------------------------------------------------------------
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TimelineItem *cell = (TimelineItem *)[self.tableView cellForRowAtIndexPath:indexPath];
    
//    if(self != self.manager.currentTimeline) {
//        return;
//    }
    
    // NSLog(@"open no notif");
    [self toggleOpenArticle:cell animated:YES];
    
    // [cell play:nil];
}

// --------------------------------------------------------------
//                           Deselect
// --------------------------------------------------------------
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // TimelineItem *cell = (TimelineItem *)[tableView cellForRowAtIndexPath:indexPath];
    // NSLog(@"didDeselectRowAtIndexPath");
    // [cell closeWithAnimation:YES];
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}


- (void)toggleOpenArticle:(TimelineItem *)cell animated:(BOOL)animated
{
    
//    NSLog(@"%u cell isOpen", cell.isOpen);
//    NSLog(@"%u cell isOpening", cell.isOpening);
//    NSLog(@"%u cell isClosing", cell.isClosing);
//    
//    NSLog(@"%u currentTimelineItem isOpen", self.currentTimelineItem.isOpen);
//    NSLog(@"%u currentTimelineItem isOpening", self.currentTimelineItem.isOpening);
//    NSLog(@"%u currentTimelineItem isClosing", self.currentTimelineItem.isClosing);
    
    if(cell.isOpening || cell.isClosing) {
        NSLog(@"cell is already opening or closing...");
        return;
    }
    
    
    if (self.currentTimelineItem){
        NSLog(@"self.currentTimelineItem");
        if(!self.currentTimelineItem.isOpening && !self.currentTimelineItem.isClosing){
            NSLog(@"1");
            [self.currentTimelineItem closeWithAnimation:YES];
        }
        else {
            NSLog(@"2");
            return;
        }
    }
    else {
        NSLog(@"no self.currentTimelineItem");
    }
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    
    if (self.selectedNumber == indexPath.row) {
        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
        self.selectedNumber = -1;
        self.currentTimelineItem = nil;
        // self.manager.currentArticle = nil;
    }
    else {
        self.selectedNumber = indexPath.row;
        [cell openWithAnimation:animated];
        self.currentTimelineItem = cell;
        
        self.manager.currentArticle = self.currentTimelineItem.data.article;
    
    }
    
    
    // TODO
    [self.tableView beginUpdates];
    [self.tableView endUpdates];
    
//    if (animated) {
//        [self.tableView beginUpdates];
//        [self.tableView endUpdates];
//    }
//    else {
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedNumber inSection:0];
//        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
//    }
    
    
    NSArray *paths = [self.tableView indexPathsForVisibleRows];
    NSInteger indexOfFirstVisibleCell = [paths[0] row];
    NSInteger indexOfLastVisibleCell = [paths[paths.count - 1] row];
    
    if(indexPath.row == indexOfFirstVisibleCell) {
        NSLog(@"scroll top");
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    if(indexPath.row == indexOfLastVisibleCell) {
        NSLog(@"scroll bottom");
        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}



// --------------------------------------------------------------
//                       Go to Article
// --------------------------------------------------------------
- (void)goToArticle:(TimelineItem *)cell
{
    
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    BOOL firstLoad = NO;
    
    if(self.articleViewController == nil){
        self.articleViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"Article"];
        firstLoad = YES;
    }

    if(self.articleDetailList == nil){
        self.articleDetailList = [self.storyboard instantiateViewControllerWithIdentifier:@"ArticlesDetailListXib"];
        // firstLoad = YES;
    }
    
    if(cell.isOpening || cell.isClosing || cell.isOpeningToArticleDetail) {
        NSLog(@"-------- goToArticle busy");
        NSLog(@"%u", cell.isOpening);
        NSLog(@"%u", cell.isClosing);
        NSLog(@"%u", cell.isOpeningToArticleDetail);
        
        return;
    }
    
    if(!cell.isOpen) {
        cell.willOpenToArticleDetail = YES;
        [self toggleOpenArticle:cell animated:YES];
        NSLog(@"must open");
        return;
    }
    
    cell.isOpeningToArticleDetail = YES;
    
    // TO DO
    [self.manager.mainViewController.view addSubview:self.articleDetailList.view];
    
    self.articleDetailList.view.hidden = NO;
    
    // array of article
    NSMutableArray *articlesTemp = [NSMutableArray new];
    
    for (TimelineItemData *itemData in self.timelineItemDataList) {
        [articlesTemp addObject:itemData.article];
    }
    
    NSArray *articles = articlesTemp;
    
    [self.articleDetailList initArticlesDetailListWithArticles:articles goToIndex:indexPath.row];
    
    self.articleDetailList.scrollViewConstraintTopCloseState = cell.frame.origin.y - self.tableView.contentOffset.y + 64;
    self.articleDetailList.scrollViewConstraintBottomCloseState = [[UIScreen mainScreen] bounds].size.height - self.articleDetailList.scrollViewConstraintTopCloseState - 320;
    
    
    [self.articleDetailList animateFromTimelineWithIndex:indexPath.row];
    
}


- (void)setupRefreshControl
{
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    self.refreshControl.backgroundColor = [UIColor whiteColor];
    
    UIImage *image = [UIImage imageNamed:@"timeline-refresh"];
    self.spinner = [[UIImageView alloc] initWithImage:image];
    self.spinner.frame = CGRectMake(([[UIScreen mainScreen] bounds].size.width - image.size.width) / 2, 15, image.size.width, image.size.height);
    [self.refreshControl addSubview:self.spinner];
    
    // Hide the original spinner icon
    self.refreshControl.tintColor = [UIColor clearColor];
    self.refreshControl.backgroundColor = Rgb2UIColor(237, 241, 244);
    
    [self.refreshControl addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
}

- (void)refresh:(id)sender
{
    
    [self runSpinAnimationOnView:self.spinner duration:1 rotations:1 repeat:10];
    
    // request API...
    
    double delayInSeconds = 3.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        
        NSLog(@"DONE");
        
        [self.refreshControl endRefreshing];
        [self.spinner.layer removeAllAnimations];
    });
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    //--- pull to refresh
    CGFloat percent = self.tableView.contentOffset.y * 100 / -119;
    CGFloat angle = percent * 360 / 100;
    
    if (percent <= 100) {
        self.spinner.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(angle));
    }
    //---
    
}

- (void)runSpinAnimationOnView:(UIView*)view duration:(CGFloat)duration rotations:(CGFloat)rotations repeat:(float)repeat;
{
    CABasicAnimation *rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 /* full rotation*/ * rotations * duration ];
    rotationAnimation.duration = duration;
    rotationAnimation.cumulative = YES;
    rotationAnimation.repeatCount = repeat;
    
    [view.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
}


- (void)updateUI
{
    NSArray *cells = [self.tableView visibleCells];
    
    for (TimelineItem *cell in cells) {
        [cell updateUI];
    }
}


- (void)openArticleAtIndex:(NSInteger)index animated:(BOOL)animated {

    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    TimelineItem *cell = (TimelineItem *)[self.manager.currentTimeline.tableView cellForRowAtIndexPath:indexPath];
    if (!cell.isOpen && !cell.isOpening) {
        [self toggleOpenArticle:cell animated:animated];
        
    }
    
}


- (void)closeArticleAtIndex:(NSInteger)index {
    
    if (index == -1){
        index = self.selectedNumber;
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    TimelineItem *cell = (TimelineItem *)[self.manager.currentTimeline.tableView cellForRowAtIndexPath:indexPath];
    if (cell.isOpen) {
        [self toggleOpenArticle:cell animated:YES];
    }
    else {
        NSLog(@"ERROR closeArticleAtIndex");
    }
    
}


- (void)playArticleAtIndex:(NSInteger)index {
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
    
    TimelineItem *cell = (TimelineItem *)[self.manager.currentTimeline.tableView cellForRowAtIndexPath:indexPath];
    [cell.data.article onClickPlayBtnFromTimeline:cell setArticleDetail:nil];
}


- (void)playNextArticle {
    // self.selectedNumber += 1;
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.selectedNumber + 1 inSection:0];
    
    [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
    [self playArticleAtIndex:self.selectedNumber + 1];
}

- (void)afterAnimationFromArticleDetail {
    [self updateUI];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return NO;
}
*/


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
