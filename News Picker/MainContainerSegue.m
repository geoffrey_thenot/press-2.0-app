//
//  MainContainerSegue.m
//  News Picker
//
//  Created by THENOT Geoffrey on 26/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "MainContainerSegue.h"
#import "MainViewController.h"
#import "PinchOutCollectionViewController.h"

@implementation MainContainerSegue


- (void) perform
{
    // MainViewController *mainViewController = (MainViewController *) self.sourceViewController;
    UIViewController *destinationController = (UIViewController *) self.destinationViewController;
    
    destinationController.view.alpha = 0.0;
//    
//    // Add view to placeholder view
//   // mainViewController.currentViewController = destinationController;
//    [mainViewController.placeholderView addSubview: destinationController.view];
//    [mainViewController addChildViewController:destinationController];
//    
//    // Set autoresizing
//    [mainViewController.placeholderView setTranslatesAutoresizingMaskIntoConstraints:NO];
//    
//    UIView *childview = destinationController.view;
//    [childview setTranslatesAutoresizingMaskIntoConstraints: NO];
//    
//    // fill horizontal
//    [mainViewController.placeholderView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat: @"H:|[childview]|" options: 0 metrics: nil views: NSDictionaryOfVariableBindings(childview)]];
//    
//    // fill vertical
//    [mainViewController.placeholderView addConstraints:[ NSLayoutConstraint constraintsWithVisualFormat: @"V:|-0-[childview]-0-|" options: 0 metrics: nil views: NSDictionaryOfVariableBindings(childview)]];
//    
//    [mainViewController.placeholderView layoutIfNeeded];
//    
//    [UIView animateWithDuration:0.5
//                          delay:0.0
//                        options:UIViewAnimationOptionTransitionFlipFromBottom
//                     animations:^{
//                         destinationController.view.alpha = 1.0;
//                         
//                     }
//                     completion:^(BOOL finished){
//                         for (int i = 0; i < (mainViewController.placeholderView.subviews.count - 1); i++) {
//                             [mainViewController.placeholderView.subviews[i] removeFromSuperview];
//                         }
//                     }];
    
}
@end

