//
//  OppositeCoverVerticalSegue.m
//  News Picker
//
//  Created by THENOT Geoffrey on 12/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "OppositeCoverVerticalSegue.h"

@implementation OppositeCoverVerticalSegue

- (void)perform
{
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    
    // Add the destination view as a subview, temporarily
    [sourceViewController.view addSubview:destinationViewController.view];
    
    [destinationViewController.view setTransform:CGAffineTransformMakeTranslation(0, -(destinationViewController.view.frame.size.height))];
    NSLog(@"BEFORE ANIM");
    [UIView animateWithDuration:0.8
                          delay:0.0
         usingSpringWithDamping:0.6
          initialSpringVelocity:10
                        options:UIViewAnimationOptionTransitionFlipFromTop //UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [destinationViewController.view setTransform:CGAffineTransformMakeTranslation(0,0)];
                     }
                     completion:^(BOOL finished){
                         [sourceViewController presentViewController:destinationViewController animated:NO completion:nil];
                         [destinationViewController.view removeFromSuperview];
                         NSLog(@"COMPLETE ANIM");
                     }];
}
@end
