//
//  TwitterViewController.m
//  News Picker
//
//  Created by geoffrey thenot on 23/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "TwitterViewController.h"
#import "NPColor.h"

@interface TwitterViewController ()

@end

@implementation TwitterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor NPYellowTranslucid];
    // Do any additional setup after loading the view.
}


- (IBAction)dismissView:(id)sender {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [self.delegate dismissViewController];
}

@end
