//
//  Timeline.h
//  News Picker
//
//  Created by JASMIN Guillaume on 12/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "TimelineItem.h"
#import "ArticleDetailList.h"

@interface Timeline : UITableViewController <AVSpeechSynthesizerDelegate, UIScrollViewDelegate>

//- (void)toggleOpenArticle:(TimelineItem *)cell;
- (void)toggleOpenArticle:(TimelineItem *)cell animated:(BOOL)animated;
- (void)goToArticle:(TimelineItem *)cell;
- (void)updateUI;
- (void)openArticleAtIndex:(NSInteger)index animated:(BOOL)animated;
- (void)closeArticleAtIndex:(NSInteger)index;
- (void)playNextArticle;
- (void)afterAnimationFromArticleDetail;
- (void)playArticleAtIndex:(NSInteger)index;

//- (void)pauseArticleNotification:(NSNotification *)item;
//- (void)endSpeech:(NSNotification *)item;

@property (strong, nonatomic) TimelineItem *currentTimelineItem;
@property (strong, nonatomic) ArticleDetailList *articleDetailList;
@property (strong, nonatomic) UIView *transitionView;
@property (assign, nonatomic) BOOL playInContinue;

-(void)initWithDay:(NSInteger)day month:(NSInteger)month year:(NSInteger)year;

@end
