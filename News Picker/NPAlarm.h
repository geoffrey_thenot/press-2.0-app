//
//  NPAlarm.h
//  News Picker
//
//  Created by THENOT Geoffrey on 25/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface NPAlarm : NSObject

@property(strong, nonatomic) NSDate *date;
@property Boolean isActive;


@end
