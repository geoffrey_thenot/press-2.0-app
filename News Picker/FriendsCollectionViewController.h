//
//  FriendsCollectionViewController.h
//  News Picker
//
//  Created by geoffrey thenot on 07/01/2015.
//  Copyright (c) 2015 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsCollectionViewController : UICollectionViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@property(nonatomic, readwrite) NSArray *friends;
@end
