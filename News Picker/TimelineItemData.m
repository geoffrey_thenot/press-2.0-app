//
//  TimelineItemData.m
//  News Picker
//
//  Created by JASMIN Guillaume on 27/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "TimelineItemData.h"
#import "Helper.h"

@implementation TimelineItemData

- (id)initWithArticle:(Article *)article {
    
    self.article = article;
    self.article.playerState     = PlayerStateToPlay;
    self.article.playerIconState = PlayerIconStatePlay;
    
    self.imageIsLoad    = NO;
    self.imageIsLoading = NO;
    self.isOpening      = NO;
    self.isClosing      = NO;
    
    self.article.image = [UIImage imageNamed:@"timeline-item-default"];
    self.article.imageFiltered = [UIImage imageNamed:@"timeline-item-default-filter"];
    
    return self;
}

//- (void)endPlaying {
//    // [self.article endPlaying];
////    self.article.playerState = PlayerStateToReplay;
////    self.article.cursorState = CursorStateToReplay;
//}

@end
