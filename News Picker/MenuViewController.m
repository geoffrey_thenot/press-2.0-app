//
//  MenuViewController.m
//  News Picker
//
//  Created by THENOT Geoffrey on 13/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "MenuViewController.h"
#import "NPAlarm.h"

#import "AccountViewController.h"
#import "LoginViewController.h"
#import "SubscriptionViewController.h"

#import <CoreData/CoreData.h>
#import "AppDelegate.h"

#import <Parse/Parse.h>

#import "ManagerData.h"

@interface MenuViewController () <AlarmContextDelegate, LoginContextDelegate, SubscribeContextDelegate>

@property NSArray *availableIdentifiers;
@property (weak, nonatomic) IBOutlet UIView *accountContainer;
@property (nonatomic, readwrite) UIViewController *accountViewController;
@property (strong, nonatomic) IBOutlet UIButton *settingsButton;
@property (nonatomic, readwrite) BOOL accountIsOpen;
@property (weak, nonatomic) IBOutlet UIButton *dateButton;

@property (nonatomic, readwrite) LoginViewController *loginVc;
@property (nonatomic, readwrite) LoginViewController *subscribeVc;
@property (nonatomic, strong) NSManagedObject *alarm;
@property (weak, nonatomic) IBOutlet UIButton *categorieCountButton;
@property (weak, nonatomic) IBOutlet UIButton *friendsButton;

@property (strong, nonatomic) ManagerData *manager;
@end

@implementation MenuViewController{
    NSFetchedResultsController* fetchedResultsController;
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.availableIdentifiers = @[@"FirstVcIdentifier", @"SecondVcIdentifier", @"ThirdVcIdentifier"];
        self.accountViewController = nil;
        self.accountIsOpen = NO;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.userName.text = @"";
    
    if([self.tabBarButtons count]) {
            
        [self performSegueWithIdentifier: @"FirstVcIdentifier"
                                  sender: self.tabBarButtons[0]];
    }
    
    [self initCloseGesture];

    [self initAccountSettings];
    
    PFUser *currentUser =  [PFUser currentUser];
    if (currentUser) {
        [self loadFacebookData];
        [self.friendsButton setTitle:@"39" forState:UIControlStateNormal];
    }
    
    [self initAlarmLabel];
    
    self.manager = [ManagerData sharedManager];
    self.manager.totalCategoriesButton = self.categorieCountButton;
}

- (void) setSelectedIndex:(int) index
{
    if ([self.tabBarButtons count] <= index) return;
    
    [self performSegueWithIdentifier: self.availableIdentifiers[index]
                              sender: self.tabBarButtons[index]];
}

-(void)prepareForSegue:(UIStoryboardSegue *) segue sender:(id) sender
{
    for (UIButton *btn in self.tabBarButtons) {
        if(sender != nil && ![btn isEqual: sender]) {
            [btn setSelected: NO];
            [self setButtonNormal:btn];
        } else if(sender != nil) {
            [btn setSelected: YES];
            [self setButtonSelected:btn];
        }
    }
}

-(void)setButtonSelected:(UIButton *)btn
{
    [UIView transitionWithView:self.view
                      duration:0.25
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                         btn.titleLabel.font = [UIFont systemFontOfSize:25];
                    }
                    completion:^(BOOL finished){
                    }];

    
}

-(void)setButtonNormal:(UIButton *) btn
{
    [UIView transitionWithView:self.view
                      duration:0.25
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        btn.titleLabel.font = [UIFont systemFontOfSize:19];
                    }
                    completion:^(BOOL finished){
                    }];
}

-(void)initCloseGesture
{
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc]
                                                 initWithTarget:self action:@selector(userSwiped:)];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    [self.view addGestureRecognizer:swipeRecognizer];
}

//Action method
- (void)userSwiped:(UIGestureRecognizer *)sender
{
    //[self performSegueWithIdentifier: @"DismissSegue" sender: self];
    [self dismissMenu:nil];
}


- (IBAction)dismissMenu:(id)sender {
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view setTransform:CGAffineTransformMakeTranslation(0,-(self.view.frame.size.height))];
                     }
                     completion:^(BOOL finished){
                         [self.view removeFromSuperview];
                     }];
}



-(void)initAccountSettings
{
    [self.accountContainer setHidden:YES];
}

-(void)openAccountSettings
{
    PFUser *currentUser =  [PFUser currentUser];
    
    self.accountIsOpen = YES;
    if (currentUser) {
        if (self.accountViewController == nil) {
            self.accountViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountVc"];
            //[self addChildViewController:self.accountViewController];
        }
        [self.accountContainer addSubview:self.accountViewController.view];
        NSLog(@"add account subview");
    } else {
        if (self.loginVc == nil) {
            self.loginVc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVc"];
            self.loginVc.delegate = self;
        }
        [self.accountContainer addSubview:self.loginVc.view];
    }
    
    [self.accountContainer setTransform:CGAffineTransformMakeTranslation(0, -self.accountContainer.frame.size.height - 190)];
    [self.accountContainer setHidden:NO];
    [UIView animateWithDuration:0.8
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.accountContainer setTransform:CGAffineTransformMakeTranslation(0, 0)];
                         [self.settingsButton setImage:[UIImage imageNamed:@"settings-close"] forState:UIControlStateNormal];
                     }
                     completion:^(BOOL finished){
                     }];
}

-(void)closeAccountSettings
{
    self.accountIsOpen = NO;
    [UIView animateWithDuration:0.8
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.accountContainer setTransform:CGAffineTransformMakeTranslation(0, -CGRectGetHeight(self.accountContainer.frame) - 190 )];
                         [self.settingsButton setImage:[UIImage imageNamed:@"menu-settings"] forState:UIControlStateNormal];
                     }
                     completion:^(BOOL finished){
                         [self.accountContainer setHidden:YES];
                         [self.loginVc.view removeFromSuperview];
                         [self.subscribeVc.view removeFromSuperview];
                         [self.accountViewController.view removeFromSuperview];
                     }];
}
- (IBAction)toggleAccountSettings:(id)sender {
    if (self.accountIsOpen) {
        [self closeAccountSettings];
    } else {
        [self openAccountSettings];
    }
}

-(void)initAlarmVC:(AlarmViewController *)alarmVC {
    self.alarmVC = alarmVC;
    self.alarmVC.delegate = self;
}

-(void)alarmDidChange:(NSString *)date {
    NSLog(@"date change: %@", date);
    [UIView transitionWithView:self.dateButton
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.dateButton setTitle:date forState:UIControlStateNormal];
                    } completion:nil];
}

// delegate login @ subscribe

-(void)loginClick {
    if (self.loginVc == nil) {
        self.loginVc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginVc"];
        [self addChildViewController:self.loginVc];
        self.loginVc.delegate = self;
    }
    NSLog(@"add login delegate subview");
    self.loginVc.view.alpha = 0;
    [self.accountContainer addSubview:self.loginVc.view];
    [UIView transitionWithView:self.loginVc.view
                      duration:0.4
                       options:UIViewAnimationOptionCurveEaseInOut
                    animations:^{
                        self.loginVc.view.alpha = 1;
                    } completion:^(BOOL finished) {
                        [self.subscribeVc.view removeFromSuperview];
                    }];
}

-(void)subscribeClick {
    NSLog(@"add subscribe subview");
    if (self.subscribeVc == nil) {
        self.subscribeVc = [self.storyboard instantiateViewControllerWithIdentifier:@"subscriptionVc"];
        //[self addChildViewController:self.subscribeVc];
        self.subscribeVc.delegate = self;
    }
    
    self.subscribeVc.view.alpha = 0;
    [self.accountContainer addSubview:self.subscribeVc.view];
    [UIView transitionWithView:self.loginVc.view
                      duration:0.4
                       options:UIViewAnimationOptionCurveEaseInOut
                    animations:^{
                        self.subscribeVc.view.alpha = 1;
                    } completion:^(BOOL finished) {
                        [self.loginVc.view removeFromSuperview];
                    }];
}

-(void)signupSuccess {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Welcome !" message:@"Welcome to News Picker, your signup was successfully saved!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alertView show];
    [self closeAccountSettings];
    [self loadFacebookData];
}

-(void)loginSuccess {
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [self closeAccountSettings];
    });
    
    [self loadFacebookData];
    [self performSegueWithIdentifier: @"FirstVcIdentifier"
                              sender: self.tabBarButtons[0]];
    [self.friendsButton setTitle:@"39" forState:UIControlStateNormal];
}

- (void)loadFacebookData {
    FBRequest *request = [FBRequest requestForMe];
    [request startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // result is a dictionary with the user's Facebook data
            NSDictionary *userData = (NSDictionary *)result;
            NSString *facebookID = userData[@"id"];
            NSString *name = userData[@"name"];
            
            self.userName.text = [[name componentsSeparatedByString:@" "] objectAtIndex:0];
            
            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
            
            NSURLRequest *urlRequest = [NSURLRequest requestWithURL:pictureURL];
            
            // Run network request asynchronously
            [NSURLConnection sendAsynchronousRequest:urlRequest
                                               queue:[NSOperationQueue mainQueue]
                                   completionHandler:
             ^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                 if (connectionError == nil && data != nil) {
                     // TO DO Set the image in the header imageView
                     self.userImage.image = [UIImage imageWithData:data];
                 }
             }];
        }
    }];
}


- (void)createOverlayImage {
    
    NSLog(@"call overlay");
    CIContext *coreImageContext = [CIContext contextWithOptions:nil];
    CIFilter *gradientFilter = [CIFilter filterWithName:@"CILinearGradient"];
    
    [gradientFilter setDefaults];
    
    CIColor *startColor   = [CIColor colorWithCGColor:[UIColor clearColor].CGColor];
    CIColor *endColor     = [CIColor colorWithCGColor:Rgb2UIColorAlpha(0, 0, 0, 0.7).CGColor];
    CIVector *startVector = [CIVector vectorWithX:0 Y:200];
    CIVector *endVector   = [CIVector vectorWithX:0 Y:0];
    
    [gradientFilter setValue:startVector forKey:@"inputPoint0"];
    [gradientFilter setValue:endVector   forKey:@"inputPoint1"];
    [gradientFilter setValue:startColor  forKey:@"inputColor0"];
    [gradientFilter setValue:endColor    forKey:@"inputColor1"];
    
    CIImage *outputImage = [gradientFilter outputImage];
    CGImageRef cgimg = [coreImageContext createCGImage:outputImage fromRect:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 360)];
    
    UIImage *blackImage = [UIImage imageWithCGImage:cgimg];
    
    
    // ---
    
    CGSize newImageSize = CGSizeMake(self.userImage.image.size.width, self.userImage.image.size.height);
    UIGraphicsBeginImageContextWithOptions(newImageSize, NO, [[UIScreen mainScreen] scale]);

    [self.userImage.image drawAtPoint:CGPointMake(roundf((newImageSize.width - self.userImage.image.size.width) / 2),
                                        roundf((newImageSize.height - self.userImage.image.size.height) / 2))];
    
    [blackImage drawAtPoint:CGPointMake(roundf((newImageSize.width - blackImage.size.width) / 2),
                                        roundf((newImageSize.height - blackImage.size.height) / 2))];
    
    UIImage *imageFinal = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.userImage.image = imageFinal;
}

-(void)initAlarmLabel {
    AppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appDelegate managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:context];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:YES];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];
        
    fetchedResultsController = [[NSFetchedResultsController alloc]
                                initWithFetchRequest:fetchRequest
                                managedObjectContext:context
                                sectionNameKeyPath:nil
                                cacheName:nil];
        
        
    NSError *error;
    BOOL success = [fetchedResultsController performFetch:&error];
    if(!success) {
        NSLog(@"There was ab error fetching data: %@", error);
    }
    NSDate *date = [NSDate date];
        
    if ( [[fetchedResultsController fetchedObjects] count] > 0) {
        self.alarm = [[fetchedResultsController fetchedObjects] lastObject];
            
        date = [self.alarm valueForKey:@"date"];
        if (date == nil) {
            date = [NSDate date];
        }
    }
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponent = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond fromDate:date];
    [self.dateButton setTitle:[NSString stringWithFormat:@"%02ld:%02ld", (long)dateComponent.hour, (long)dateComponent.minute] forState:UIControlStateNormal];

}

@end

