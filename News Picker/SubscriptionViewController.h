//
//  SubscriptionViewController.h
//  News Picker
//
//  Created by THENOT Geoffrey on 07/01/2015.
//  Copyright (c) 2015 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol SubscribeContextDelegate;
@interface SubscriptionViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;
@property (strong, nonatomic) IBOutlet UITextField *passwordRepeatField;
@property (strong, nonatomic) IBOutlet UITextField *emailField;

- (IBAction)signup:(id)sender;
@property (nonatomic, assign) id<SubscribeContextDelegate> delegate;
@end

@protocol SubscribeContextDelegate <NSObject>
@required
- (void)loginClick;
-(void)signupSuccess;
@end
