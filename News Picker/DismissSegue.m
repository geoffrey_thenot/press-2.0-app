//
//  DismissSegue.m
//  News Picker
//
//  Created by THENOT Geoffrey on 12/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "DismissSegue.h"

@implementation DismissSegue

- (void)perform {
    UIViewController *sourceViewController = self.sourceViewController;
    UIViewController *destinationViewController = self.destinationViewController;
    
    [sourceViewController.view.superview insertSubview:destinationViewController.view atIndex:0];
    
    [destinationViewController.view setAlpha:1.0];
    [sourceViewController.view setTransform:CGAffineTransformMakeTranslation(0,0)];
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionTransitionFlipFromBottom
                     animations:^{
                         [sourceViewController.view setTransform:CGAffineTransformMakeTranslation(0,-(sourceViewController.view.frame.size.height))];
                     }
                     completion:^(BOOL finished){
                         [sourceViewController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
                         [destinationViewController.view removeFromSuperview];
                     }];
    
    
}

@end
