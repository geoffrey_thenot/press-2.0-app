//
//  CategoryTableViewCell.m
//  News Picker
//
//  Created by THENOT Geoffrey on 13/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "CategoryTableViewCell.h"
#import "Helper.h"


@implementation CategoryTableViewCell

- (void)awakeFromNib {
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options:UIViewAnimationOptionTransitionFlipFromBottom
                         animations:^{
                             self.cellView.backgroundColor = [UIColor NPLightGray];
                             self.titleLabel.textColor = [UIColor NPGray];
                             self.selectedImage.alpha = 0.0;
                             [self.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Regular" size:16]];
                         }
                         completion:^(BOOL finished){
                 
                         }];
        
    } else {
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options:UIViewAnimationOptionTransitionFlipFromBottom
                         animations:^{
                             self.cellView.backgroundColor = [UIColor whiteColor];
                             self.titleLabel.textColor = [UIColor NPDarkGray];
                             self.selectedImage.alpha = 1.0;
                             [self.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:16]];
                         }
                         completion:^(BOOL finished){
                             
                         }];
    }
}

@end
