//
//  TimelineItemData.h
//  News Picker
//
//  Created by JASMIN Guillaume on 27/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Article.h"


@interface TimelineItemData : NSObject

// --------------------------------------------
//              Methods
// --------------------------------------------
- (id)initWithArticle:(Article *)article;
//- (void)endPlaying;


// --------------------------------------------
//              Properties
// --------------------------------------------

@property (strong, nonatomic) Article *article;

@property (assign, nonatomic) BOOL isOpen;
@property (assign, nonatomic) BOOL isOpening;
@property (assign, nonatomic) BOOL isClosing;
@property (assign, nonatomic) BOOL isplaying;
@property (assign, nonatomic) BOOL imageIsLoad;
@property (assign, nonatomic) BOOL imageIsLoading;

@end
