//
//  TimelineItem.m
//  News Picker
//
//  Created by JASMIN Guillaume on 12/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "TimelineItem.h"
#import "ManagerData.h"

#import "Helper.h"

//#define LAST_ICON_PLAY_NUMBER @"00014"
//#define LAST_ICON_PAUSE_NUMBER @"00014"
//#define LAST_ICON_REPLAY_NUMBER @"00027"


@interface TimelineItem()

@property (strong, nonatomic) ManagerData *manager;
@property (strong, nonatomic) UIImageView *imageViewFilter;



@end

@implementation TimelineItem



- (void)awakeFromNib {
    // Initialization code

    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    self.manager = [ManagerData sharedManager];
    
    // self.data.article.startTimePlay = 0.0;
    // self.data.article.offsetTimePlay = 0.0;
    
    self.yellowLayer.backgroundColor = Rgb2UIColor(255, 240, 0);

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    
}

- (void)firstInit {
    
}

- (void)updateData:(TimelineItemData *)timelineItemData {
    
    // NSLog(@"UPDATE");
    
    self.manager = [ManagerData sharedManager];
    
    self.data = timelineItemData;
    self.data.article.timelineItem = self;
    
    [self updateIcon];
    [self hydrateArticle];
    
    
    if(self.data.isOpening) {
        self.data.isOpen = YES;
        // self.data.isOpening = NO;
    }
    
    if (self.data.isOpen) {
        self.manager.currentTimeline.currentTimelineItem = self;
    }

    self.bgImage.image           = timelineItemData.article.image;
    self.bgImageWithFilter.image = timelineItemData.article.imageFiltered;
    
    if (self.data.article.playerState == PlayerStateToContinueFromExistingUtterance || self.data.article.playerState == PlayerStateToContinueFromNewUtterance) {
        self.data.article.playerState = PlayerStateToContinueFromNewUtterance;
    }
    
    self.bgImageWithFilter.layer.mask = self.data.article.maskLayerLarge;
    self.yellowLayer.layer.mask = self.data.article.maskLayerSmall;
    
    // mask
    self.bgImageWithFilter.layer.masksToBounds = NO;
    self.yellowLayer.layer.masksToBounds = NO;
    
    self.clipsToBounds = YES;
    self.bgImageWithFilter.clipsToBounds = YES;
    
    [self.data.article loadUtterance];
    [self updateUI];
}

- (void)updateUI
{
    self.iconHighlighted.hidden = !self.data.article.isHighlighted;
    
    self.bgImageWithFilter.layer.mask = self.data.article.maskLayerLarge;
    
}

// ----------------------------------------------------------------
//                          open
// ----------------------------------------------------------------
- (void)openWithAnimation:(BOOL)animated {
    
    if(self.isOpening) {
        NSLog(@"already open");
        NSLog(@"isOpening %u", self.isOpening);
        NSLog(@"isClosing %u", self.isClosing);
        NSLog(@"isOpen %u", self.isOpen);
        
        return;
    }
    
    // NSLog(@"opening... %@", self.data.article.PFArticle[@"title"]);
    
    self.isOpening = YES;
    self.data.isOpening = self.isOpening;
    
    [self.largeTitle sizeToFit];
    [self updateIcon];
    
    [self.openBtn setSelected:YES];
    
    [self layoutIfNeeded];
    
    self.constraintSmallTitleTop.constant = -60;
    self.constraintSmallCategoryTop.constant = -100;

    self.largeCategory.hidden = NO;
    self.largeTitle.hidden = NO;

    self.constraintLargeTitleBottom.constant = 20;
    self.constraintLargeCategoryBottom.constant = self.largeTitle.frame.size.height + 20;

    if(animated){
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options:UIViewAnimationOptionTransitionNone | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.opacityFilter.alpha = 1;
                             [self layoutIfNeeded];
                             
                         }
                         completion:^(BOOL finished){
                             
                             
                             self.isOpening = NO;
                             self.data.isOpening = NO;
                             self.isOpen = YES;
                             self.data.isOpen = YES;
                             
                             if (self.willOpenToArticleDetail) {
                                 self.willOpenToArticleDetail = NO;
                                 [self goToArticleDetail:nil];
                             }
                             
                         }];
    
    }
    else {
        self.opacityFilter.alpha = 1;
        [self layoutIfNeeded];

        self.isOpening = NO;
        self.isOpen = YES;
        
        self.data.isOpening = self.isOpening;
        self.data.isOpen = self.isOpen;
        
        if (self.willOpenToArticleDetail) {
            self.willOpenToArticleDetail = NO;
            [self goToArticleDetail:nil];
        }
    }
    
}

// ----------------------------------------------------------------
//                          Close
// ----------------------------------------------------------------
- (void)closeWithAnimation:(BOOL)animated {
    
//    if(self.isClosing || !self.isOpen){
    if(self.isClosing){
        NSLog(@"closeWithAnimation already");
        NSLog(@"isClosing %u", self.isClosing);
        NSLog(@"isOpen %u", self.isOpen);
        return;
    }
    
    self.isClosing = YES;
    
    // NSLog(@"closing... %@", self.data.article.PFArticle[@"title"]);
    
    [self.openBtn setSelected:NO];
    [self updateIcon];
    
    [self layoutIfNeeded];
    self.constraintSmallTitleTop.constant       = 35;
    self.constraintLargeTitleBottom.constant    = -200;
    self.constraintSmallCategoryTop.constant    = 0;
    self.constraintLargeCategoryBottom.constant = -40;
    
    if(animated){
        
        [UIView animateWithDuration:0.5f
                              delay:0.0f
                            options:UIViewAnimationOptionTransitionNone | UIViewAnimationOptionBeginFromCurrentState
                         animations:^{
                             self.opacityFilter.alpha = 0;
                             [self layoutIfNeeded];
                         }
                         completion:^(BOOL finished){
                             self.largeTitle.hidden = YES;
                             self.largeCategory.hidden = YES;
                             self.isClosing = NO;
                             self.isOpen = NO;
                             self.data.isOpen = NO;
                         }];
        

    }
    else {
        self.opacityFilter.alpha = 0;
        
        [self layoutIfNeeded];
        
        self.largeTitle.hidden    = YES;
        self.largeCategory.hidden = YES;
        
        self.isClosing = NO;
        self.data.isOpen = self.isOpen;
    }

}


// ----------------------------------------------------------------
//                          Play / pause action
// ----------------------------------------------------------------
- (IBAction)play:(id)sender
{
    // NSLog(@"play action");
    [self.data.article onClickPlayBtnFromTimeline:self setArticleDetail:nil];
}


- (void)pauseActionWithUpdateIcon:(BOOL)updateIcon
{
    // NSLog(@"pause action");
    [self.data.article pauseActionFromTimeline:self setArticleDetail:nil updateIcon:updateIcon];
}


- (IBAction)goToArticleDetail:(id)sender
{
    // NSLog(@"open article");
    [self.manager.currentTimeline goToArticle:self];
}

// ------
- (void)clean {
    
    self.isOpen    = NO;
    self.isOpening = NO;
    self.isClosing = NO;
    self.isOpeningToArticleDetail = NO;
    
    // [self.openBtn setSelected:NO];
    [self updateIcon];
    
    self.constraintSmallTitleTop.constant       = 35;
    self.constraintLargeTitleBottom.constant    = -200;
    self.constraintLargeCategoryBottom.constant = -40;
    self.constraintSmallCategoryTop.constant    = 0;
    
    self.opacityFilter.alpha = 0;
    self.largeTitle.hidden = YES;
    self.largeCategory.hidden = YES;
}


- (void)hydrateArticle {
    self.smallTitle.text = self.data.article.PFArticle[@"title"];
    self.largeTitle.text = self.smallTitle.text;

    self.category.text = [self.data.article.category uppercaseString];
    self.largeCategory.text = self.category.text;
}


- (void)updateIcon {
    
    // NSLog(@"Update icon");
    
    [self.manager.mainViewController updatePlayButton];
   
    NSString *suffixState;
    NSString *suffixStateBtnOpen;
    NSString *suffixStatePause;
    NSString *animatedState;
    
    if(self.isOpening) {
        suffixState = @"-white";
        suffixStatePause = @"";
        animatedState = @"open";
        suffixStateBtnOpen = @"-white";
    }
    else if(self.isClosing) {
        suffixState = @"";
        suffixStatePause = @"-black";
        animatedState = @"closed";
        suffixStateBtnOpen = @"-black";
    }
    else if(self.isOpen){
        suffixState = @"-white";
        suffixStatePause = @"";
        animatedState = @"open";
        suffixStateBtnOpen = @"-white";
    }
    else {
        suffixState = @"";
        suffixStatePause = @"-black";
        animatedState = @"closed";
        suffixStateBtnOpen = @"-black";
    }
    
    self.openBtn.imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"timeline-open%@",suffixStateBtnOpen]];

    [self.data.article updateIcon:self.playBtn setSuffixState:suffixState setSuffixStatePause:suffixStatePause setAnimatedState:animatedState];
}

- (void)callbackPlay
{
    if(!self.isOpen){
        // NSLog(@"play");
        [self.manager.currentTimeline toggleOpenArticle:self animated:YES];
    }
    else {
        [self updateIcon];
    }
}

- (void)afterAnimationFromArticleDetail {
    self.bgImageWithFilter.layer.mask = self.manager.currentArticle.maskLayerLarge;
}




@end
