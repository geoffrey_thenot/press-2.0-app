//
//  FriendCollectionViewCell.m
//  News Picker
//
//  Created by THENOT Geoffrey on 08/01/2015.
//  Copyright (c) 2015 THENOT Geoffrey. All rights reserved.
//

#import "FriendCollectionViewCell.h"
#import "NPColor.h"

@implementation FriendCollectionViewCell


- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.profilImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        self.profilImage.layer.cornerRadius = 25;
        self.profilImage.backgroundColor = [UIColor blackColor];
        self.profilImage.layer.masksToBounds = YES;
        self.profilName = [[UILabel alloc] initWithFrame:CGRectMake(65, 0, 100, 50)];
        self.profilName.font = [UIFont fontWithName:@"OpenSans-Regular" size:17];
        self.profilName.textColor = [UIColor NPDarkGray];
        [self addSubview:self.profilName];
        [self addSubview:self.profilImage];
    }
    return self;
}

@end
