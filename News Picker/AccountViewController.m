//
//  AccountViewController.m
//  News Picker
//
//  Created by geoffrey thenot on 29/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "AccountViewController.h"
#import <Parse/Parse.h>
#import <FacebookSDK/FacebookSDK.h>

@interface AccountViewController ()

@end

@implementation AccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    PFUser *currentUser =  [PFUser currentUser];
    NSLog(@"user %@", currentUser);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)logoutButtonAction:(id)sender {
    NSLog(@"LOGOUT");
    [FBSession.activeSession close];
    [FBSession setActiveSession:nil];
    [FBSession.activeSession closeAndClearTokenInformation];
    [PFUser logOut];
}

@end
