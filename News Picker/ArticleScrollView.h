//
//  ArticleScrollView.h
//  News Picker
//
//  Created by Guillaume Jasmin on 23/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleScrollView : UIScrollView <UIScrollViewDelegate>

@end
