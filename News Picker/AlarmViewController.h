//
//  AlarmViewController.h
//  News Picker
//
//  Created by THENOT Geoffrey on 24/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "ViewController.h"

#import <UIKit/UIKit.h>
#import "BEMAnalogClockView.h"

@protocol AlarmContextDelegate;

@interface AlarmViewController : UIViewController <BEMAnalogClockDelegate, UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet BEMAnalogClockView *myClock1;

@property (strong, nonatomic) NSString *dateLabel;

@property (weak, nonatomic) IBOutlet UIView *panView;

@property (nonatomic, assign) id<AlarmContextDelegate> delegate;

@end

@protocol AlarmContextDelegate <NSObject>

@required
- (void)alarmDidChange:(NSString *)date;

@end