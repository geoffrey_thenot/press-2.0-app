//
//  FriendCollectionViewCell.h
//  News Picker
//
//  Created by THENOT Geoffrey on 08/01/2015.
//  Copyright (c) 2015 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendCollectionViewCell : UICollectionViewCell

@property(nonatomic, readwrite) IBOutlet UILabel *profilName;
@property(nonatomic, readwrite) IBOutlet UIImageView *profilImage;

@end
