//
//  ArticleDetailList.h
//  News Picker
//
//  Created by Guillaume Jasmin on 31/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Article.h"
#import "ArticleViewController.h"

@interface ArticleDetailList : UIViewController <UIScrollViewDelegate>

- (void)initArticlesDetailListWithArticles:(NSArray *)articles goToIndex:(NSInteger)goToIndex;
- (void)animateFromTimelineWithIndex:(NSInteger)index;
- (void)closeArticle;

// @property (strong, nonatomic) Article *article;
@property (strong, nonatomic) ArticleViewController *currentArticleViewController;
@property (assign, nonatomic) NSInteger currentArticleIndex;


@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollViewConstraintTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollViewConstraintBottom;


@property (assign, nonatomic) NSInteger scrollViewConstraintTopOpenState;
@property (assign, nonatomic) NSInteger scrollViewConstraintBottomOpenState;
@property (assign, nonatomic) NSInteger scrollViewConstraintTopCloseState;
@property (assign, nonatomic) NSInteger scrollViewConstraintBottomCloseState;

@end
