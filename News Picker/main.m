//
//  main.m
//  News Picker
//
//  Created by THENOT Geoffrey on 31/10/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

