//
//  LoginViewController.h
//  News Picker
//
//  Created by THENOT Geoffrey on 07/01/2015.
//  Copyright (c) 2015 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
@protocol LoginContextDelegate;
@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *usernameField;
@property (strong, nonatomic) IBOutlet UITextField *passwordField;

@property (nonatomic, assign) id<LoginContextDelegate> delegate;
- (IBAction)login:(id)sender;

@end
@protocol LoginContextDelegate <NSObject>
@required
- (void)subscribeClick;
- (void)loginSuccess;
@end
