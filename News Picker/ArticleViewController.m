//
//  Article.m
//  News Picker
//
//  Created by Guillaume Jasmin on 19/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "ArticleViewController.h"
#import "Helper.h"
#import "ManagerData.h"
#import "ArticleDetailList.h"

#import "GHContextMenuView.h"
#import "UIImage+ImageEffects.h"

#import "SocialTemplateViewController.h"
#import "NSString+HTML.h"

#define DESCRIPTION_CONTAINER_BORDER_HEIGHT 75 / 2
#define DESCRIPTION_CONTAINER_BORDER_DIFF 35 / 2
#define DESCRIPTION_CONTAINER_BORDER_HEIGHT_FULL 55
#define DESCRIPTION_TEXT_MARGIN_TOP 40
#define CONTENT_TEXT_MARGIN_TOP 20

@interface ArticleViewController ()<GHContextOverlayViewDataSource, GHContextOverlayViewDelegate, SocialTemplateContextDelegate>

@property (strong, nonatomic) ManagerData *manager;

@property (nonatomic, readwrite) UIImageView *bluredImageView;
@property (nonatomic, readwrite) UIView *bluredMask;

@property (nonatomic, readwrite) SocialTemplateViewController *socialViewController;
@property (nonatomic, strong) ArticleDetailList *articleDetailList;

@property (strong, nonatomic) UILabel *highlightedInfo;
@property (strong, nonatomic) UILabel *dateLabel;


@end

@implementation ArticleViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // NSLog(@"viewDidLoad articleDetail");
    
    self.manager = [ManagerData sharedManager];
    // Do any additional setup after loading the view.
    [self initPressGesture];
    
    self.manager.articleController = self;
        
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self updateIcon];

}

- (void)viewDidLayoutSubviews {}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)initMaskValue
{
    self.maskHeight      = 360;
    self.maskStartPointY = 0;
}


- (void)initWithArticle:(Article *)article animated:(BOOL)animated {
    
    self.manager = [ManagerData sharedManager];
    
    [self initMaskValue];
    
    self.imageContainer.layer.zPosition = 1;
    
    // title & category & description
    self.article = article;
    self.article.articleViewController = self;
    self.articleTitle.text = self.article.PFArticle[@"title"];
    self.articleTitleSmall.text = self.articleTitle.text;
    self.articleCategory.text = [self.article.category uppercaseString];
    
    self.article.playBtn = self.playBtn;

    [self setupArticleDescription];
    [self setupArticleContent];
    [self setupLettrine];
    
    self.iconHighlighted.hidden = !self.article.isHighlighted;
    
    [self.articleTitle sizeToFit];
    [self.articleDescription sizeToFit];
    [self.articleTitleSmall sizeToFit];
    [self.articleContent sizeToFit];
    
    self.contentViewConstraintHeight.constant = self.imageContainer.frame.size.height + self.articleContent.frame.size.height + DESCRIPTION_CONTAINER_BORDER_HEIGHT_FULL + CONTENT_TEXT_MARGIN_TOP;
    
    if (self.contentViewConstraintHeight.constant < [[UIScreen mainScreen] bounds].size.height){
        self.contentViewConstraintHeight.constant = [[UIScreen mainScreen] bounds].size.height;
    }
    
    self.descriptionContainerConstraintHeight.constant = self.articleDescription.frame.size.height + DESCRIPTION_TEXT_MARGIN_TOP + DESCRIPTION_CONTAINER_BORDER_HEIGHT_FULL;
    
    [self setupMaskDescriptionContainer];
    

    // image
    UIImage *image;
    UIImage *imageFiltered;
    if (self.article.imageData == nil) {
        image = [UIImage imageNamed:@"timeline-item-default"];
        imageFiltered = [UIImage imageNamed:@"timeline-item-default-filter"];
    }
    else {
        // image = [UIImage imageWithData:self.article.imageData];
        image = self.article.image;
        imageFiltered = self.article.imageFiltered;
    }
    
    self.articleImage.image = image;
    self.articleImageFiltered.image = imageFiltered;
    
    [self rotateCloseBtn:CloseBtnStateToOpen animated:animated];

    self.contentView.clipsToBounds    = YES;
    self.articleImage.clipsToBounds   = YES;
    self.articleImageFiltered.clipsToBounds   = YES;
    self.imageContainer.clipsToBounds = YES;
    
    [self setupImageContainer];
    
    [self addDate];
    
    UITapGestureRecognizer *tapGestureDescription = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickDescription)];
    [self.descriptionContainer addGestureRecognizer:tapGestureDescription];
    
    [self setupLayerImageFilter];
    
}

- (void)setupImageContainer
{
    // image mask
    CAShapeLayer *smallMaskLayer = [CAShapeLayer layer];
    UIBezierPath *smallFilterPath = [UIBezierPath bezierPath];
    [smallFilterPath moveToPoint:CGPointMake(0, 0)];
    [smallFilterPath addLineToPoint:CGPointMake([[UIScreen mainScreen] bounds].size.width, 0)];
    [smallFilterPath addLineToPoint:CGPointMake([[UIScreen mainScreen] bounds].size.width, 320)];
    [smallFilterPath addLineToPoint:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, 320 + DESCRIPTION_CONTAINER_BORDER_DIFF)];
    [smallFilterPath addLineToPoint:CGPointMake(0, 320)];
    [smallFilterPath closePath];
    
    smallMaskLayer.backgroundColor = [[UIColor clearColor] CGColor];
    smallMaskLayer.path = [smallFilterPath CGPath];
    
//    self.articleImage.layer.mask = smallMaskLayer;
//    self.articleImage.layer.masksToBounds = NO;

    self.imageContainer.layer.mask = smallMaskLayer;
    self.imageContainer.layer.masksToBounds = NO;
    
    self.articleTitleSmall.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGestureSmallArticleTitle = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onClickSmallTitle)];
    [self.articleTitleSmall addGestureRecognizer:tapGestureSmallArticleTitle];
}

- (void)setupLayerImageFilter
{
    self.articleImageFiltered.layer.mask = self.article.maskLayerLarge;
}

- (void)rotateCloseBtn:(CloseBtnState)closeBtnState animated:(BOOL)animated
{
    
    if(animated){
        
        CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotationAnimation.removedOnCompletion = NO;
        rotationAnimation.fillMode = kCAFillModeForwards;
    
        if(closeBtnState == CloseBtnStateToOpen){
            rotationAnimation.fromValue = [NSNumber numberWithFloat: DEGREES_TO_RADIANS(0)];
            rotationAnimation.toValue   = [NSNumber numberWithFloat: DEGREES_TO_RADIANS(180)];
        }
        else {
            rotationAnimation.fromValue = [NSNumber numberWithFloat: DEGREES_TO_RADIANS(180)];
            rotationAnimation.toValue   = [NSNumber numberWithFloat: DEGREES_TO_RADIANS(0)];
        }
    
        rotationAnimation.duration = 0.5;
        rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:@"linear"];
    
        [self.closeBtn.layer addAnimation:rotationAnimation forKey:@"rotationAnimation"];
    }
    else {
        
        if(closeBtnState == CloseBtnStateToOpen){
            self.closeBtn.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(180));
        }
        else {
            self.closeBtn.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(0));
        }
    }
}

- (IBAction)closeArticle:(id)sender {
    [self.manager.currentTimeline.articleDetailList closeArticle];
}

- (IBAction)playAction:(id)sender {
    // NSLog(@"--- play");
    [self.article onClickPlayBtnFromTimeline:nil setArticleDetail:self];
}

- (void)setupMaskDescriptionContainer
{
    CAShapeLayer *smallMaskLayer = [CAShapeLayer layer];
    UIBezierPath *smallFilterPath = [UIBezierPath bezierPath];
    [smallFilterPath moveToPoint:CGPointMake(0, 0)];
    [smallFilterPath addLineToPoint:CGPointMake([[UIScreen mainScreen] bounds].size.width, 0)];
    [smallFilterPath addLineToPoint:CGPointMake([[UIScreen mainScreen] bounds].size.width, self.descriptionContainerConstraintHeight.constant - DESCRIPTION_CONTAINER_BORDER_DIFF)];
    [smallFilterPath addLineToPoint:CGPointMake([[UIScreen mainScreen] bounds].size.width / 2, self.descriptionContainerConstraintHeight.constant)];
    [smallFilterPath addLineToPoint:CGPointMake(0, self.descriptionContainerConstraintHeight.constant - DESCRIPTION_CONTAINER_BORDER_DIFF)];
    [smallFilterPath closePath];
    
    smallMaskLayer.backgroundColor = [[UIColor clearColor] CGColor];
    smallMaskLayer.path = [smallFilterPath CGPath];
    
    self.descriptionContainer.layer.mask = smallMaskLayer;
    self.descriptionContainer.layer.masksToBounds = NO;
    self.descriptionContainer.backgroundColor = Rgb2UIColor(255, 240, 0);
 
    [self.view layoutIfNeeded];
    [self.descriptionContainer layoutIfNeeded];
    
    self.descriptionContainerConstraintTop.constant = 320 - self.descriptionContainerConstraintHeight.constant + DESCRIPTION_CONTAINER_BORDER_HEIGHT_FULL;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


/****************************************************\
        Long press gesture
\****************************************************/

-(void)initPressGesture
{
    GHContextMenuView* overlay = [[GHContextMenuView alloc] init];
    overlay.dataSource = self;
    overlay.delegate = self;
    
    UILongPressGestureRecognizer* _longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:overlay action:@selector(longPressDetected:)];

    [self.view addGestureRecognizer:_longPressRecognizer];
}

- (void)initBluredImage
{
    self.bluredImageView = [[UIImageView  alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    
    [self.bluredImageView setContentMode:UIViewContentModeScaleToFill];
    //self.bluredImageView.image = [self blurWithCoreImage:[self takeSnapshotOfView:[self view]]];
    self.bluredImageView.image = [self blurWithImageEffects:[self takeSnapshotOfView:[self view]]];
    
    self.bluredMask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame))];
    self.bluredMask.backgroundColor = [UIColor whiteColor];
    self.bluredImageView.layer.mask = self.bluredMask.layer;
}

- (NSInteger)numberOfMenuItems
{
    return 4;
}

- (void)menuIsVisible:(BOOL)visible
{
    if (visible) {
        [self initBluredImage];
        [self.view addSubview:self.bluredImageView];
    } else {
        [self.bluredImageView removeFromSuperview];
    }
}

-(UIImage *)imageForItemAtIndex:(NSInteger)index
{
    NSString* imageName = nil;
    switch (index) {
        case 0:
            imageName = @"star";
            break;
        case 1:
            imageName = @"facebook";
            break;
        case 2:
            imageName = @"twitter";
            break;
        case 3:
            imageName = @"mail";
            break;
            
        default:
            break;
    }
    return [UIImage imageNamed:imageName];
}

-(UIImage *)imageHighlightForItemAtIndex:(NSInteger)index
{
    NSString* imageName = @"highlight";
    return [UIImage imageNamed:imageName];
}

- (void)didSelectItemAtIndex:(NSInteger)selectedIndex forMenuAtPoint:(CGPoint)point
{
    switch (selectedIndex) {
        case 0:
            [self presentSocialViewControllerWithIdentifier:@"StarVc"];
            break;
        case 1:
            [self presentSocialViewControllerWithIdentifier:@"FacebookVc"];
            break;
        case 2:
            [self presentSocialViewControllerWithIdentifier:@"TwitterVc"];
            break;
        case 3:
            [self presentSocialViewControllerWithIdentifier:@"MailVc"];
            break;
            
        default:
            break;
    }
}

-(void)presentSocialViewControllerWithIdentifier:(NSString *)identifier
{
    //[self initBluredImage];
    [self.view addSubview:self.bluredImageView];
    self.socialViewController = [self.storyboard instantiateViewControllerWithIdentifier:identifier];
    self.socialViewController.delegate = self;
    self.socialViewController.view.backgroundColor = [UIColor clearColor];
    [self addChildViewController:self.socialViewController];
    
    [self.view addSubview:self.socialViewController.view];
    [self.socialViewController.view setAlpha:0.0];
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.socialViewController.view setAlpha:1.0];
                     }
                     completion:^(BOOL finished){

                     }];
    
}

- (UIImage *)takeSnapshotOfView:(UIView *)view
{
    UIGraphicsBeginImageContext(CGSizeMake(view.frame.size.width, view.frame.size.height));
    [view drawViewHierarchyInRect:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height) afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (UIImage *)blurWithImageEffects:(UIImage *)image
{
    return [image applyBlurWithRadius:2 tintColor:[UIColor colorWithWhite:1 alpha:0.1] saturationDeltaFactor:1.2 maskImage:nil];
}

- (void)onClickSmallTitle
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}

- (void)onClickDescription {
    // NSLog(@"open description");
    
    CGFloat openHeight = self.descriptionContainerConstraintHeight.constant - DESCRIPTION_CONTAINER_BORDER_HEIGHT_FULL;
    
    UIImage *image;
    
    // close
    if (self.descriptionIsOpen) {
        self.descriptionContainerConstraintTop.constant = self.descriptionContainerConstraintTop.constant - openHeight;
        self.contentViewConstraintHeight.constant = self.contentViewConstraintHeight.constant - openHeight;
        image = [UIImage imageNamed:@"article-sample"];
    }
    
    // open
    else {
        self.descriptionContainerConstraintTop.constant = self.descriptionContainerConstraintTop.constant + openHeight;
        self.contentViewConstraintHeight.constant = self.contentViewConstraintHeight.constant + openHeight;
        image = [UIImage imageNamed:@"article-sample-close"];
    }
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                         self.descriptionIsOpen = !self.descriptionIsOpen;
                         self.iconDescriptionState.image = image;
                     }];
}

-(void)dismissViewController
{
    [self.bluredImageView removeFromSuperview];
    [self.socialViewController.view removeFromSuperview];
}

- (void)setupLettrine
{
    NSString *firstChar = [[self.articleContent.text substringToIndex:1] lowercaseString];
    NSString *lettrineName = [NSString stringWithFormat:@"lettrine-%@", firstChar];
    UIImage *lettrineImage = [UIImage imageNamed:lettrineName];
    
    self.lettrineConstraintWidth.constant = lettrineImage.size.width;
    self.lettrineConstraintHeight.constant = lettrineImage.size.height;
    self.lettrine.image = lettrineImage;
    
    CGRect lettrineFrame = CGRectMake(0, 0, lettrineImage.size.width + 10, lettrineImage.size.height + 5 - 15);
    UIBezierPath *lettrinePath = [UIBezierPath bezierPathWithRect:lettrineFrame];
    self.articleContent.textContainer.exclusionPaths = @[lettrinePath];
    
    self.articleContent.text = [self.articleContent.text stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@""];
}

- (void)setupArticleDescription
{
    
    self.descriptionIsOpen = NO;
    self.iconDescriptionState.image = [UIImage imageNamed:@"article-sample"];
    
    UIFont *font = [UIFont fontWithName:@"Rooney-Heavy" size:15];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    // paragraphStyle.headIndent = 15;
    // paragraphStyle.firstLineHeadIndent = 15;
    paragraphStyle.lineSpacing = 10;
    
    NSDictionary *attrsDictionary = @{ NSFontAttributeName:font,
                                       NSParagraphStyleAttributeName:paragraphStyle };
    
    self.articleDescription.attributedText = [[NSAttributedString alloc] initWithString:self.article.PFArticle[@"description"] attributes:attrsDictionary];
}

- (void)setupArticleContent
{
    NSString *content = self.article.PFArticle[@"content"];
    
    content = [content stringByReplacingOccurrencesOfString:@"</p>" withString:@"</p>[[**]]"];
    content = [content stringByConvertingHTMLToPlainText];
    content = [content stringByReplacingOccurrencesOfString:@"[[**]] " withString:@"[[**]]"];
    content = [content stringByReplacingOccurrencesOfString:@"[[**]]" withString:@"\n\n"];
    
    self.articleContent.selectable = NO;
    
    UIFont *font = [UIFont fontWithName:@"Rooney-Regular" size:14];
    
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    // paragraphStyle.headIndent = 15;
    // paragraphStyle.firstLineHeadIndent = 15;
    paragraphStyle.lineSpacing = 5;
    
    NSDictionary *attrsDictionary = @{ NSFontAttributeName:font,
                                       NSParagraphStyleAttributeName:paragraphStyle };
    
    self.articleContent.attributedText = [[NSAttributedString alloc] initWithString:content attributes:attrsDictionary];
    
    CGSize sizeThatShouldFitTheContent = [self.articleContent sizeThatFits:self.articleContent.frame.size];
    self.articleContentConstraintHeight.constant = sizeThatShouldFitTheContent.height;
}

- (void)setHighlighted:(BOOL)isHighlighted
{
    self.article.isHighlighted = isHighlighted;
    self.iconHighlighted.hidden = !self.article.isHighlighted;
}

- (void)animateFromTimeline
{
    [self.view layoutIfNeeded];
    self.iconHighlightedConstraintTop.constant = 30;
    
    self.manager.mainViewController.currentArticleContext = CurrentArticleContextDetail;
    [self updateIcon];
    
    [UIView animateWithDuration:0.5f
                          delay:0.0f
                        options:UIViewAnimationOptionTransitionNone | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

- (void)updateIcon
{
    NSString *suffixState = @"-white";
    NSString *suffixStatePause = @"";
    NSString *animatedState = @"open";
    
    [self.article updateIcon:self.playBtn setSuffixState:suffixState setSuffixStatePause:suffixStatePause setAnimatedState:animatedState];
}

- (void)callbackPlay
{
    [self updateIcon];
}

- (void)addHighlightedInfo
{
    
    self.articleContentConstraintTop.constant += 30;
    self.lettrineConstraintTop.constant += 30;
    [self.contentView layoutIfNeeded];
    
    self.highlightedInfo = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, [[UIScreen mainScreen] bounds].size.width, 20)];
    
    self.highlightedInfo.text = @"Highlighted by Geoffrey and Julie";
    self.highlightedInfo.textColor = Rgb2UIColor(160, 163, 180);
    self.highlightedInfo.font = [UIFont fontWithName:@"OpenSans-Bold" size:14];
    self.highlightedInfo.textAlignment = NSTextAlignmentCenter;
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(65, 15, 11, 11)];
    imageView.image = [UIImage imageNamed:@"article-highlithed"];
    
    [self.textContainer addSubview:self.highlightedInfo];
    [self.textContainer addSubview:imageView];
    
}
- (void)addDate
{
    
    NSInteger posY = 10;
    
    if(self.article.isHighlightedByPeople) {
        [self addHighlightedInfo];
        
        posY = 40;
    }
    
    self.articleContentConstraintTop.constant += 30;
    self.lettrineConstraintTop.constant += 30;
    [self.contentView layoutIfNeeded];
    
    self.dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, posY, [[UIScreen mainScreen] bounds].size.width, 20)];
    
    NSDate *dateOfTimeline = self.article.PFArticle[@"date"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE, d MMM yyyy - hh:mm a"];
    NSString *formattedDateString = [[dateFormatter stringFromDate:dateOfTimeline] uppercaseString];
    
    self.dateLabel.text = formattedDateString;
    self.dateLabel.textColor = Rgb2UIColor(160, 163, 180);
    self.dateLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:14];
    self.dateLabel.textAlignment = NSTextAlignmentCenter;
    
    [self.textContainer addSubview:self.dateLabel];
}


@end
