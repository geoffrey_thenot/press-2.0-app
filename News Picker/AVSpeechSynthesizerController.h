//
//  AVSpeechSynthesizerController.h
//  News Picker
//
//  Created by JASMIN Guillaume on 24/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import "TimelineItem.h"
#import "TimelineItemData.h"

@interface AVSpeechSynthesizerController : AVSpeechSynthesizer <AVSpeechSynthesizerDelegate>

@property (strong, nonatomic) TimelineItem *currentTimelineItemPlay;
@property (strong, nonatomic) TimelineItemData *currentTimelineItemDataPlay;


@end
