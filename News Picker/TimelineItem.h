//
//  TimelineItem.h
//  News Picker
//
//  Created by JASMIN Guillaume on 12/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
//#import <AVFoundation/AVFoundation.h>
#import "TimelineItemData.h"

@interface TimelineItem : UITableViewCell


- (IBAction)play:(id)sender;
- (IBAction)goToArticleDetail:(id)sender;

- (void)updateData:(TimelineItemData *)timelineItemData;
- (void)openWithAnimation:(BOOL)animated;
- (void)closeWithAnimation:(BOOL)animated;
- (void)clean;
- (void)hydrateArticle;
- (void)pauseActionWithUpdateIcon:(BOOL)updateIcon;
- (void)updateUI;
- (void)callbackPlay;
- (void)updateIcon;
- (void)afterAnimationFromArticleDetail;

// data
@property (strong, nonatomic) TimelineItemData *data;

// btn, label...
@property (strong, nonatomic) IBOutlet UILabel     *smallTitle;
@property (strong, nonatomic) IBOutlet UILabel     *largeTitle;
@property (strong, nonatomic) IBOutlet UILabel     *category;
@property (strong, nonatomic) IBOutlet UILabel     *largeCategory;
@property (strong, nonatomic) IBOutlet UIImageView *bgImage;
@property (strong, nonatomic) IBOutlet UIImageView *bgImageWithFilter;
@property (strong, nonatomic) IBOutlet UIButton    *openBtn;
@property (strong, nonatomic) IBOutlet UIButton    *playBtn;
@property (strong, nonatomic) IBOutlet UIView      *opacityFilter;
@property (strong, nonatomic) IBOutlet UIView      *yellowLayer;
@property (strong, nonatomic) IBOutlet UIImageView *iconHighlighted;

@property (assign, nonatomic) BOOL isOpen;
@property (assign, nonatomic) BOOL isOpening;
@property (assign, nonatomic) BOOL isClosing;
@property (assign, nonatomic) BOOL isOpeningToArticleDetail;
@property (assign, nonatomic) BOOL willOpenToArticleDetail;

// constrainte
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintSmallTitleTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintLargeTitleBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintSmallCategoryTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *constraintLargeCategoryBottom;

@property (assign, nonatomic) CGFloat smallMaskMarginArc;
@property (assign, nonatomic) CGFloat maskHeight;
@property (assign, nonatomic) CGFloat smallMaskHeight;
@property (assign, nonatomic) CGFloat maskStartPointY;
@property (assign, nonatomic) CGFloat smallMaskStartPointY;







@end
