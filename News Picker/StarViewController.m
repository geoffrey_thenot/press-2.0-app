//
//  StarViewController.m
//  News Picker
//
//  Created by geoffrey thenot on 23/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "StarViewController.h"
#import "NPColor.h"
#import "ManagerData.h"

@interface StarViewController ()

@property (strong, nonatomic) ManagerData *manager;

@end

@implementation StarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.manager = [ManagerData sharedManager];
    
    self.view.backgroundColor = [UIColor NPYellowTranslucid];
    
    self.articleTitle.text = self.manager.currentArticle.PFArticle[@"title"];
}

- (IBAction)dismissView:(id)sender {
    [self.view removeFromSuperview];
    [self removeFromParentViewController];
    [self.delegate dismissViewController];
}

- (IBAction)highlightArticle:(id)sender {
    // [self.manager.articleController setHighlighted:YES];
    [self.manager.currentTimeline.articleDetailList.currentArticleViewController setHighlighted:YES];
    [self dismissView:nil];
}

@end
