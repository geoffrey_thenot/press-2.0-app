//
//  ViewController.m
//  News Picker
//
//  Created by THENOT Geoffrey on 31/10/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "ViewController.h"

#import <AVFoundation/AVFoundation.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIButton *test;
@property (strong, nonatomic) AVSpeechSynthesizer *synthesizer;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *testFrench;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSLog(@"%f", [[UIScreen mainScreen] bounds].size.height);
    
    self.synthesizer = [[AVSpeechSynthesizer alloc] init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)testSpeach:(id)sender {
    AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:self.textView.text];
    utterance.rate = 0.3;
    //utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-GB"];
    utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"en-au"];
    [self.synthesizer speakUtterance:utterance];
}

- (IBAction)testFrenchSpeach:(id)sender {
    AVSpeechUtterance *utterance = [[AVSpeechUtterance alloc] initWithString:self.textView.text];
    utterance.rate = 0.3;
   
    utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:@"fr-CA"];
    [self.synthesizer speakUtterance:utterance];
}

@end
