//
//  Article.h
//  News Picker
//
//  Created by Guillaume Jasmin on 19/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import "ArticleScrollView.h"
#import "Article.h"


@interface ArticleViewController : UIViewController

typedef enum {
    CloseBtnStateToOpen,
    CloseBtnStateToClose
} CloseBtnState;

- (IBAction)closeArticle:(id)sender;
- (IBAction)playAction:(id)sender;
- (void)initWithArticle:(Article *)article animated:(BOOL)animated;
- (void)setHighlighted:(BOOL)isHighlighted;
- (void)animateFromTimeline;
- (void)rotateCloseBtn:(CloseBtnState)closeBtnState animated:(BOOL)animated;
- (void)updateIcon;
- (void)callbackPlay;

@property (strong, nonatomic) Article *article;


@property (assign, nonatomic) NSInteger scrollViewConstrainteTopOpenState;
@property (assign, nonatomic) NSInteger scrollViewConstrainteBottomOpenState;
@property (assign, nonatomic) NSInteger scrollViewConstrainteTopCloseState;
@property (assign, nonatomic) NSInteger scrollViewConstrainteBottomCloseState;

@property (assign, nonatomic) BOOL descriptionIsOpen;

// UI
@property (strong, nonatomic) IBOutlet ArticleScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView            *contentView;
@property (strong, nonatomic) IBOutlet UIView            *descriptionContainer;
@property (strong, nonatomic) IBOutlet UIView            *imageContainer;
@property (strong, nonatomic) IBOutlet UIButton          *playBtn;
@property (strong, nonatomic) IBOutlet UIButton          *closeBtn;
@property (strong, nonatomic) IBOutlet UILabel           *articleTitle;
@property (strong, nonatomic) IBOutlet UILabel           *articleCategory;
@property (strong, nonatomic) IBOutlet UILabel           *articleDescription;

@property (strong, nonatomic) IBOutlet UITextView *articleContent;
@property (strong, nonatomic) IBOutlet UIImageView       *articleImage;
@property (strong, nonatomic) IBOutlet UIImageView       *articleImageFiltered;
@property (strong, nonatomic) IBOutlet UILabel           *articleTitleSmall;

@property (strong, nonatomic) IBOutlet UIImageView *iconDescriptionState;
@property (strong, nonatomic) IBOutlet UIImageView *lettrine;
@property (strong, nonatomic) IBOutlet UIImageView *iconHighlighted;

// constraint
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *contentViewConstraintHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *imageContainerConstraintTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollViewConstrainteTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *scrollViewConstrainteBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *articleTitleConstraintBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *smallArticleTitleConstraintTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *closeBtnConstraintBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *playBtnConstraintBottom;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descriptionContainerConstraintTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *descriptionContainerConstraintHeight;
@property (strong, nonatomic) IBOutlet UIView *textContainer;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *articleContentConstraintHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lettrineConstraintWidth;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lettrineConstraintHeight;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *iconHighlightedConstraintTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *articleContentConstraintTop;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *lettrineConstraintTop;

@property (assign, nonatomic) CGFloat maskHeight;
@property (assign, nonatomic) CGFloat maskStartPointY;


@end
