//
//  SubscriptionViewController.m
//  News Picker
//
//  Created by THENOT Geoffrey on 07/01/2015.
//  Copyright (c) 2015 THENOT Geoffrey. All rights reserved.
//

#import "SubscriptionViewController.h"
#import <Parse/Parse.h>
#import "SuperParseTwitter.h"
#import <ParseFacebookUtils/PFFacebookUtils.h>

@interface SubscriptionViewController ()

@end

@implementation SubscriptionViewController


- (IBAction)goToLogin:(id)sender {
    [self.delegate loginClick];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.usernameField setDelegate:self];
    [self.passwordField setDelegate:self];
    [self.passwordRepeatField setDelegate:self];
    [self.emailField setDelegate:self];
}


- (IBAction)signup:(id)sender {
    NSString *username = [self.usernameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [self.passwordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *passwordRepeat = [self.passwordRepeatField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *email = [self.emailField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([username length] == 0 || [password length] == 0 || [passwordRepeat length] == 0 || [email length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Please enter a Username, Password and Email Adress!"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    } else if (![password isEqualToString:passwordRepeat]) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"'Confirm password' and 'Password' do not match"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    } else {
        PFUser *newUser = [PFUser user];
        newUser.username = username;
        newUser.password = password;
        newUser.email = email;
        [newUser  signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (!error) {
                [self.delegate signupSuccess];
            } else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:[error userInfo][@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.usernameField) {
        [textField resignFirstResponder];
        [self.passwordField becomeFirstResponder];
    } else if (textField == self.passwordField) {
        [textField resignFirstResponder];
        [self.passwordRepeatField becomeFirstResponder];
    } else if (textField == self.passwordRepeatField) {
        [textField resignFirstResponder];
        [self.emailField becomeFirstResponder];
    } else if (textField == self.emailField) {
        [self signup:nil];
    }
    
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (IBAction)facebookSignup:(id)sender {
    NSArray *permissionsArray = @[ @"user_about_me", @"user_friends", @"user_groups"];
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        if (!user) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:[error userInfo][@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        } else {
            [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (!error) {
                    // Store the current user's Facebook ID on the user
                    [[PFUser currentUser] setObject:[result objectForKey:@"id"]
                                             forKey:@"fbId"];
                    [[PFUser currentUser] saveInBackground];
                }
            }];
            if (user.isNew) {
                [self.delegate signupSuccess];
            } else {
                [self.delegate signupSuccess];
            }
        }
    }];
}

- (IBAction)twitterSignup:(id)sender {
    [SuperParseTwitter logInWithView:self.view Block:^(PFUser *user, NSError *error) {
        if (!user) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"Twitter wasn't enabled in your settings" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return;
        } else if (user.isNew) {
            [self.delegate signupSuccess];
        } else {
            [self.delegate signupSuccess];
        }
    }];

}



@end
