//
//  PinchOutCollectionViewController.m
//  News Picker
//
//  Created by THENOT Geoffrey on 26/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "PinchOutCollectionViewController.h"
#import "TimelineCollectionViewCell.h"
#import "Helper.h"

#import "LineLayout.h"

static NSString * const TimelineCellIdentifier = @"TimelineCell";
@interface PinchOutCollectionViewController ()

@end

@implementation PinchOutCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.timelines = [[NSArray alloc] initWithObjects:@"test", @"resetset", @"tetsetsea", nil];
    
    [self initCollectionView];
    [self initPinchOut];
}

-(void)initCollectionView {
    // Register cell classes

    [self.collectionView registerClass:[TimelineCollectionViewCell class]
            forCellWithReuseIdentifier:TimelineCellIdentifier];

    [self.collectionView setScrollEnabled:YES];
    [self.collectionView setPagingEnabled:YES];
    self.collectionView.collectionViewLayout = [[LineLayout alloc] init];

    self.collectionView.backgroundColor = [UIColor NPDarkGray];
    
    [self.view addSubview:self.collectionView];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSLog(@"%li", (unsigned long)self.timelines.count);
    return [self.timelines count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    // NSString *timeline = self.timelines[indexPath.item];
    // Configure the cell
    TimelineCollectionViewCell *timelineCell = [collectionView dequeueReusableCellWithReuseIdentifier:TimelineCellIdentifier
                                                                            forIndexPath:indexPath];
    
    
    return timelineCell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TimelineCollectionViewCell *timelineCell = (TimelineCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [UIView
     animateWithDuration:0.2
     animations:^{
         timelineCell.frame = self.view.window.bounds;
     }];
}

#pragma mark gesture
-(void)initPinchOut
{
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchWithGestureRecognizer:)];

    [self.view addGestureRecognizer:pinchRecognizer];
    
}

-(void)handlePinchWithGestureRecognizer:(UIPinchGestureRecognizer *)pinchGestureRecognizer{
    self.collectionView.transform = CGAffineTransformScale(self.collectionView.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
     pinchGestureRecognizer.scale = 1.0;
    
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(260, 390);
}

@end
