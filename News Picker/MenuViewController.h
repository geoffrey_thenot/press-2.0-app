//
//  MenuViewController.h
//  News Picker
//
//  Created by THENOT Geoffrey on 13/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Helper.h"
#import "AlarmViewController.h"

@interface MenuViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *placeholderView;

@property (nonatomic, strong) UIViewController *currentViewController;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *tabBarButtons;
@property (nonatomic, strong) AlarmViewController *alarmVC;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

- (void) setSelectedIndex: (int) index;
-(void)initAlarmVC:(AlarmViewController*)alarmVC;

@end
