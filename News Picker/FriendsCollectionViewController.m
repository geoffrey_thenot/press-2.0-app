//
//  FriendsCollectionViewController.m
//  News Picker
//
//  Created by geoffrey thenot on 07/01/2015.
//  Copyright (c) 2015 THENOT Geoffrey. All rights reserved.
//

#import "FriendsCollectionViewController.h"
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>

#import "FriendCollectionViewCell.h"
#import "NPColor.h"

@interface FriendsCollectionViewController ()
@property (nonatomic) UIButton *loginButton;
@end

@implementation FriendsCollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    self.view.backgroundColor = [UIColor whiteColor];
    // Register cell classes
    [self.collectionView registerClass:[FriendCollectionViewCell class] forCellWithReuseIdentifier:@"friendCell"];
    CGRect frame = self.collectionView.frame;
    frame.origin.y += 30;
    frame.size.height -= 30;
    self.collectionView.frame = frame;
    [self initButtons];
    
    PFUser *currentUser =  [PFUser currentUser];
    if (currentUser) {
        [self getFacebookFriends];
    } else {
        UILabel *titleLogin = [[UILabel alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 64)];
        [titleLogin setTextColor:[UIColor NPDarkGray]];
        [titleLogin setFont:[UIFont fontWithName:@"OpenSans-Regular" size:17]];
        [titleLogin setText:@"You're not Log in :("];
        [titleLogin setTextAlignment:NSTextAlignmentCenter];
        
        self.loginButton = [[UIButton alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.view.frame) / 2) - 100, 104, 200, 64)];
        self.loginButton.backgroundColor = [UIColor NPYellow];
        [self.loginButton setTitleColor:[UIColor NPDarkGray] forState:UIControlStateNormal];
        [self.loginButton setTitle:@"LOG IN" forState:UIControlStateNormal];
        [self.loginButton.titleLabel setFont:[UIFont fontWithName:@"OpenSans-Bold" size:17]];
        
        [self.view addSubview:self.loginButton];
        [self.view addSubview:titleLogin];
    }
}
#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.friends count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    FriendCollectionViewCell *friendCell = [collectionView
                                    dequeueReusableCellWithReuseIdentifier:@"friendCell"
                                    forIndexPath:indexPath];
    
    NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", self.friends[indexPath.row][@"id"]]];
    friendCell.profilImage.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:pictureURL]];
    friendCell.profilName.text = [[self.friends[indexPath.row][@"name"] componentsSeparatedByString:@" "] objectAtIndex:0];
    

    return friendCell;
}


#pragma mark facebook utilities

-(void)getFacebookFriends {
    [FBRequestConnection startWithGraphPath:@"/188256048005473/members"
                                 parameters:nil
                                 HTTPMethod:@"GET"
                          completionHandler:^(
                                              FBRequestConnection *connection,
                                              id result,
                                              NSError *error
                                              ) {
                              self.friends = [result objectForKey:@"data"];
                              [self.collectionView reloadData];
                          }];
}

-(void)initButtons {
    UIButton *plusButton = [[UIButton alloc]initWithFrame:CGRectMake(10, 0, 40, 40)];
    [plusButton setImage:[UIImage imageNamed:@"plus-friends"] forState:UIControlStateNormal];
    
    UIButton *editButton = [[UIButton alloc]initWithFrame:CGRectMake(self.view.frame.size.width - 50, 0, 40, 40)];
    [editButton setImage:[UIImage imageNamed:@"edit-friends"] forState:UIControlStateNormal];
    
    [self.view addSubview:plusButton];
    [self.view addSubview:editButton];
}

@end
