//
//  AVSpeechSynthesizerController.m
//  News Picker
//
//  Created by JASMIN Guillaume on 24/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "AVSpeechSynthesizerController.h"
#import "ManagerData.h"

@interface AVSpeechSynthesizerController ()

@property (strong, nonatomic) ManagerData *manager;

@end

@implementation AVSpeechSynthesizerController

- (id)init {
    
    self.delegate = self;
    return self;
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer willSpeakRangeOfSpeechString:(NSRange)characterRange utterance:(AVSpeechUtterance *)utterance {
//    self.currentTimelineItemDataPlay.article.characterRangeLocationRemaining = [self.currentTimelineItemDataPlay.article.currentVoiceString length] - characterRange.location;
    
    self.manager = [ManagerData sharedManager];
    
    // self.manager.currentArticle.characterRangeLocationRemaining = [self.manager.currentArticle.currentVoiceString length] - characterRange.location;
    self.manager.currentArticlePlaying.characterRangeLocationRemaining = [self.manager.currentArticlePlaying.currentVoiceString length] - characterRange.location;
}

- (void)speechSynthesizer:(AVSpeechSynthesizer *)synthesizer didFinishSpeechUtterance:(AVSpeechUtterance *)utterance {
    NSLog(@"end speech");
    self.manager = [ManagerData sharedManager];
    [self stopSpeakingAtBoundary:NO];
    
    // [self.manager.currentArticle endPlaying];
    [self.manager.currentArticlePlaying endPlaying];
}

@end
