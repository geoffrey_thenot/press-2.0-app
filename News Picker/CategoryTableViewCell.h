//
//  CategoryTableViewCell.h
//  News Picker
//
//  Created by THENOT Geoffrey on 13/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImage;
@property (weak, nonatomic) IBOutlet UIView *cellView;

@end
