//
//  MenuTabBarSegue.m
//  News Picker
//
//  Created by THENOT Geoffrey on 13/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "MenuTabBarSegue.h"
#import "MenuViewController.h"
#import "AlarmViewController.h"

@implementation MenuTabBarSegue



- (void) perform
{
    MenuViewController *menuViewController = (MenuViewController *) self.sourceViewController;
    UIViewController *destinationController = (UIViewController *) self.destinationViewController;
    if ([self.destinationViewController isKindOfClass:[AlarmViewController class]]) {
        AlarmViewController *destinationController = (AlarmViewController *) self.destinationViewController;
        [menuViewController initAlarmVC:destinationController];
        NSLog(@"alarm init");
    }
    
    destinationController.view.alpha = 0.0;
    
    // Add view to placeholder view
    menuViewController.currentViewController = destinationController;
    [menuViewController.placeholderView addSubview: destinationController.view];
    
    // Set autoresizing
    [menuViewController.placeholderView setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    UIView *childview = destinationController.view;
    [childview setTranslatesAutoresizingMaskIntoConstraints: NO];
    
    // fill horizontal
    [menuViewController.placeholderView addConstraints: [NSLayoutConstraint constraintsWithVisualFormat: @"H:|[childview]|" options: 0 metrics: nil views: NSDictionaryOfVariableBindings(childview)]];
    
    // fill vertical
    [menuViewController.placeholderView addConstraints:[ NSLayoutConstraint constraintsWithVisualFormat: @"V:|-0-[childview]-0-|" options: 0 metrics: nil views: NSDictionaryOfVariableBindings(childview)]];
    
    [menuViewController.placeholderView layoutIfNeeded];
    
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionTransitionFlipFromBottom
                     animations:^{
                         destinationController.view.alpha = 1.0;
                         
                     }
                     completion:^(BOOL finished){
                         for (int i = 0; i < (menuViewController.placeholderView.subviews.count - 1); i++) {
                             [menuViewController.placeholderView.subviews[i] removeFromSuperview];
                         }
                     }];
   
}
@end
