//
//  SocialTemplateViewController.h
//  News Picker
//
//  Created by geoffrey thenot on 23/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "ViewController.h"

@protocol SocialTemplateContextDelegate;

@interface SocialTemplateViewController : ViewController

@property (nonatomic, assign) id<SocialTemplateContextDelegate> delegate;

@end

@protocol SocialTemplateContextDelegate <NSObject>

@required
- (void)dismissViewController;

@end


