//
//  CategoriesViewController.m
//  News Picker
//
//  Created by THENOT Geoffrey on 13/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "CategoriesViewController.h"
#import "CategoryTableViewCell.h"

#import "ManagerData.h"
@interface CategoriesViewController ()

@property (strong, nonatomic) ManagerData *manager;
@property (nonatomic) NSInteger countCategories;
@end

@implementation CategoriesViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        NSLog(@"load categories");
        [self loadCategories];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    
    if ([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
    
    self.manager = [ManagerData sharedManager];
}


- (void)loadCategories
{
    self.categories = [[NSMutableArray alloc] init];
    PFQuery *query = [PFQuery queryWithClassName:@"Category"];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (error) {
            NSLog(@"an error occurred on groups load");
        } else {
            NSLog(@"loaded");
            self.categories = [[NSMutableArray alloc] initWithArray:objects];
            [self.tableView reloadData];
            self.countCategories = [self.categories count];
            [self.manager.totalCategoriesButton setTitle:[@(self.countCategories) stringValue] forState:UIControlStateNormal];
        }
    }];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.categories count];
}



#pragma mark - Table view delegate source

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     CategoryTableViewCell *cell = (CategoryTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CategoryCell"];
    
//    [cell setSeparatorInset:UIEdgeInsetsZero];
    
//    if ([cell respondsToSelector:@selector(layoutMargins)]) {
//        cell.layoutMargins = UIEdgeInsetsZero;
//    }
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CategoryCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    PFObject *category = [[PFObject alloc] initWithClassName:@"Category"];
    category= [self.categories objectAtIndex:indexPath.row];
    
    cell.titleLabel.text = category[@"name"];
    

    
    return cell;
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.countCategories++;
    [self.manager.totalCategoriesButton setTitle:[@(self.countCategories) stringValue] forState:UIControlStateNormal];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.countCategories--;
    [self.manager.totalCategoriesButton setTitle:[@(self.countCategories) stringValue] forState:UIControlStateNormal];
}

@end
