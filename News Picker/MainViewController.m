//
//  MainViewController.m
//  News Picker
//
//  Created by geoffrey thenot on 11/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "MainViewController.h"
#import "Helper.h"
#import "Timeline.h"
#import "MenuViewController.h"
#import "ManagerData.h"
@interface MainViewController ()

@property (strong, nonatomic) IBOutlet UIImageView *logoImage;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property Boolean isPlaying;
@property Boolean searchIsOpen;
@property (weak, nonatomic) IBOutlet UIView *searchContainer;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@property (strong, nonatomic) IBOutlet UIView *menuContainer;
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;

@property (nonatomic, strong) NSMutableArray *timelines;
@property (nonatomic, strong) NSMutableArray *viewControllers;


@property (weak, nonatomic) MenuViewController *menuViewController;

@property (nonatomic, readwrite) CGFloat lastScale;
@property (nonatomic, readwrite) ManagerData *manager;
@end

@implementation MainViewController


/****************************************************\
            Init functions
\****************************************************/
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.isPlaying = NO;
        self.menuViewController = nil;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
//    
    UIImageView *splashScreen = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
    
    splashScreen.layer.zPosition = 1;
    
    [self.view addSubview:splashScreen];

     NSString *animatedImagesName = @"animation-splash-screen_";
     
     splashScreen.animationImages = [Helper createAnimationArray:animatedImagesName imgCout:60];
     splashScreen.animationDuration = 3;
     splashScreen.animationRepeatCount = 1;
     NSString *imageName = [NSString stringWithFormat:@"%@00060", animatedImagesName];
     splashScreen.image = [UIImage imageNamed:imageName];
     // [splashScreen setImage:splashScreen.image forState:UIControlStateNormal];
     [splashScreen startAnimating];
    
    double delayInSeconds = splashScreen.animationDuration * 5 / 6;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//        [splashScreen removeFromSuperview];
        // [self initAfterSplashScreen];
    });
    
    [self initAfterSplashScreen];

}

- (void)initAfterSplashScreen {
    
     self.manager = [ManagerData sharedManager];
     self.manager.mainViewController = self;
     
     self.currentArticleContext = CurrentArticleContextTimeline;
     
     [self initSearchView];
     [self.contentScrollView setBackgroundColor:[UIColor NPDarkGray]];
     
     [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
     
     [self.menuContainer setBackgroundColor:[UIColor NPDarkGray]];
     [self.searchContainer setBackgroundColor:[UIColor NPDarkGray]];
     [self.searchTextField setBackgroundColor:[UIColor NPDarkGray]];
     
     [self initTimelines];
     [self initPinchOut];
     [self initProgressSpeech];
    
}

/****************************************************\
            Play functions
\****************************************************/

- (IBAction)togglePlayButton:(id)sender {
    if(self.manager.synthesizer.currentTimelineItemPlay) {
        // [self.manager.synthesizer.currentTimelineItemPlay play:nil];
        // [self.manager.currentArticle onClickPlayBtnFromTimeline:self.manager.currentArticle.timelineItem setArticleDetail:nil];
        [self.manager.currentArticlePlaying onClickPlayBtnFromTimeline:self.manager.currentArticle.timelineItem setArticleDetail:nil];
    }
}

- (void)updatePlayButton {    
    if(self.manager.synthesizer.currentTimelineItemPlay.data.article.isPlaying){
        [self.playButton setImage:[UIImage imageNamed:@"topbar-pause"] forState:UIControlStateNormal];
    }
    else {
        [self.playButton setImage:[UIImage imageNamed:@"topbar-play"] forState:UIControlStateNormal];
    }
}

/****************************************************\
            search gesture
\****************************************************/

- (IBAction)searchToggle:(id)sender {
    if (self.searchIsOpen) {
        [self closeSearchView];
    } else {
        [self openSearchView];
    }
}

- (void) openSearchView
{
    [self.searchTextField becomeFirstResponder];
    self.searchIsOpen = YES;
    [self.searchContainer setHidden:NO];

    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.searchContainer setTransform:CGAffineTransformMakeTranslation(0, 0)];
                     }
                     completion:^(BOOL finished){
                         
                     }];

}

- (void) closeSearchView
{
    [self.searchTextField resignFirstResponder];
    self.searchIsOpen = NO;
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                       [self.searchContainer setTransform:CGAffineTransformMakeTranslation(0, -44)];
                     }
                     completion:^(BOOL finished){
                         [self.searchContainer setHidden:YES];
                     }];
}

- (void) initSearchView
{
    [self.searchContainer setTransform:CGAffineTransformMakeTranslation(0, -44)];
    [[self.searchTextField valueForKey:@"textInputTraits"] setValue:[UIColor NPYellow] forKey:@"insertionPointColor"];
    [self.searchTextField setValue:[UIColor NPGray] forKeyPath:@"_placeholderLabel.textColor"];
    [self.searchTextField setDelegate:self];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.searchTextField resignFirstResponder];
    
    return NO;
}

/****************************************************\
            Menu gesture
\****************************************************/

- (IBAction)openMenu:(id)sender {
    [self closeSearchView];
}


/****************************************************\
            Timeline pages
\****************************************************/

-(void)initTimelines {
    self.timelines = [[NSMutableArray alloc] initWithObjects:@"one", @"two", @"three", @"four", @"five", @"six", @"seven",nil];
    NSUInteger numberPages = [self.timelines count];
    
    // view controllers are created lazily
    // in the meantime, load the array with placeholders which will be replaced on demand
    NSMutableArray *controllers = [[NSMutableArray alloc] init];
    for (NSUInteger i = 0; i < numberPages; i++) {
        [controllers addObject:[NSNull null]];
    }
    self.viewControllers = controllers;
    
    // a page is the width of the scroll view
    self.contentScrollView.pagingEnabled = YES;
    self.contentScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame) * numberPages, CGRectGetHeight(self.contentScrollView.frame));
    self.contentScrollView.showsHorizontalScrollIndicator = YES;
    self.contentScrollView.showsVerticalScrollIndicator = NO;
    self.contentScrollView.scrollsToTop = NO;

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    // pages are created on demand
    // load the visible page
    // load the page on either side to avoid flashes when the user starts scrolling
    
    [self loadScrollViewWithPage:0];
    [self loadScrollViewWithPage:1];
    [self initDateLabelWithPage:0];
}

-(void)initDateLabelWithPage:(NSInteger)page {
    NSDate *dateOfTimeline = [NSDate dateWithTimeIntervalSinceNow:-(86400.0 * page)];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:@"EEE, d MMM yyyy"];
    NSString *formattedDateString = [[dateFormatter stringFromDate:dateOfTimeline] uppercaseString];
    
    [UIView transitionWithView:self.dateLabel
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.dateLabel setText:formattedDateString];
                    } completion:nil];
}

- (void)loadScrollViewWithPage:(NSUInteger)page
{
    if (page >= [self.timelines count])
        return;
    
    // replace the placeholder if necessary
    Timeline *controller = [self.viewControllers objectAtIndex:page];
    
    NSDate *dateOfTimeline = [NSDate dateWithTimeIntervalSinceNow:-(86400.0*page)];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *date = [calendar components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:dateOfTimeline];
    
    if ((NSNull *)controller == [NSNull null]) {
        controller = [self.storyboard instantiateViewControllerWithIdentifier:@"TimelineXib"];
        [controller initWithDay:date.day month:date.month year:date.year];
        [self.viewControllers replaceObjectAtIndex:page withObject:controller];
    }
    
    if (self.manager.currentTimeline == nil) {
        self.manager.currentTimeline = controller;
    }
    
    // add the controller's view to the scroll view
    if (controller.view.superview == nil) {
        [self initTableViewSize:controller.view page:page];
        
        //init "container" view of timeline and date
        UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame) * page, -40, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - 24)];

        // init "date" label
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        
        [dateFormatter setDateFormat:@"EEE, d MMM yyyy"];
        NSString *formattedDateString = [[dateFormatter stringFromDate:dateOfTimeline] uppercaseString];
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(containerView.frame), 40)];
        [dateLabel setText:formattedDateString];
        [dateLabel setTextColor:self.dateLabel.textColor];
        [dateLabel setFont:self.dateLabel.font];
        [containerView addSubview:dateLabel];

        [containerView addSubview:controller.view];
        
        controller.view.frame = CGRectMake(0, 40, CGRectGetWidth(containerView.frame), CGRectGetHeight(containerView.frame)-40);
        
        [self.contentScrollView addSubview:containerView];
        [self addChildViewController:controller];
        
        //   [controller didMoveToParentViewController:self];
    }
}

// at the end of scroll animation, reset the boolean used when scrolls originate from the UIPageControl
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    // switch the indicator when more than 50% of the previous/next page is visible
    CGFloat pageWidth = CGRectGetWidth(self.contentScrollView.frame);
    NSUInteger page = floor((self.contentScrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    self.manager.currentTimeline = [self.viewControllers objectAtIndex:page];
    [self initDateLabelWithPage:page];
}

- (void)gotoPage:(NSInteger)page animated:(BOOL)animated
{
    // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
    [self loadScrollViewWithPage:page - 1];
    [self loadScrollViewWithPage:page];
    [self loadScrollViewWithPage:page + 1];
    
    // update the scroll view to the appropriate page
    CGRect bounds = self.contentScrollView.bounds;
    bounds.origin.x = CGRectGetWidth(bounds) * page;
    bounds.origin.y = 0;
    [self.contentScrollView scrollRectToVisible:bounds animated:animated];

    self.manager.currentTimeline = [self.viewControllers objectAtIndex:page];
    
    [self initDateLabelWithPage:page];
}

/****************************************************\
        Pinch gesture
\****************************************************/

-(void)initPinchOut
{
    UIPinchGestureRecognizer *pinchRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchWithGestureRecognizer:)];
    
    [self.view addGestureRecognizer:pinchRecognizer];
    
}

-(void)handlePinchWithGestureRecognizer:(UIPinchGestureRecognizer *)pinchGestureRecognizer{
    UIGestureRecognizerState state = pinchGestureRecognizer.state;
  //  self.collectionView.transform = CGAffineTransformScale(self.collectionView.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
    
    
    if (state == UIGestureRecognizerStateEnded && pinchGestureRecognizer.scale >= 0.7) {
        [self resetTimelinesSizes];
        _lastScale = 1.0f;
        return;
    }
    
    if (state == UIGestureRecognizerStateEnded)  {
        _lastScale = 1.0f;
        return;
    }
    
    CGFloat scale = 1.0f - (_lastScale - pinchGestureRecognizer.scale);
    [self resizeTimelines:scale];
    
    _lastScale = pinchGestureRecognizer.scale;
    
    if (pinchGestureRecognizer.scale < 0.7) {
        [pinchGestureRecognizer setEnabled:NO];
        [self.contentScrollView setPagingEnabled:NO];
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             [self displayLogo:YES];
                             [self.view layoutIfNeeded];
                             for (int i = 0; i < [self.contentScrollView.subviews count]; ++i) {
                                 [self setTableViewSizes:self.contentScrollView.subviews[i] page:i];
                                 
                             }
                             self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.frame.size.width * 0.7 * [self.contentScrollView.subviews count], self.contentScrollView.frame.size.height);
                         }
                         completion:^(BOOL finished){
                             [pinchGestureRecognizer setEnabled:YES];
                         }];
    } else {
        NSLog(@"%f", pinchGestureRecognizer.scale);
        [self resizeTimelines:pinchGestureRecognizer.scale];
    }
    

}

-(void)resizeTimelines:(CGFloat)scale {
//    for (UIView *view in self.contentScrollView.subviews) {
//        //view.transform = CGAffineTransformScale(view.transform, (0.5 + (0.5 * scale)), (0.7 + (0.7 * scale)));
//        view.transform = CGAffineTransformScale(view.transform, scale, scale);
//    }
}

-(void)resetTimelinesSizes {
//    for (UIView *view in self.contentScrollView.subviews) {
//        view.transform = CGAffineTransformScale(view.transform, 1.0, 1.0);
//    }
}

-(void)setTableViewLeftMargin:(UIView *)view {
    
}

-(void)setTableViewSizes:(UIView *)view page:(int)page {
    CGRect frame = CGRectMake((page * 320) + 40,  120, self.contentScrollView.frame.size.width * 0.7, self.contentScrollView.frame.size.height - 120);
    view.frame = frame;
    view.tag = page;
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onViewTap:)];
    [view addGestureRecognizer:tapRecognizer];
}

-(void)onViewTap:(UITapGestureRecognizer *)tapRecognizer{
    NSInteger tag = tapRecognizer.view.tag;
    [self.contentScrollView setPagingEnabled:YES];
    [self removeTapGesturesFromViews];
    [UIView animateWithDuration:0.5
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self displayLogo:NO];
                         int count = 0;
                         for (UIView *view in self.contentScrollView.subviews) {
                             [self initTableViewSize:view page:count];
                             count++;
                         }
                         
                         self.contentScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.view.frame) * [self.contentScrollView.subviews count], CGRectGetHeight(self.contentScrollView.frame));
                         
                         [self gotoPage:tag animated:NO];
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

-(void)removeTapGesturesFromViews {
    for (UIView *view in self.contentScrollView.subviews) {
        UIGestureRecognizer *recognizer = [view.gestureRecognizers lastObject];
        [view removeGestureRecognizer:recognizer];
        
    }
}

-(void)initTableViewSize:(UIView *)view page:(NSInteger)page {
    CGRect frame = self.contentScrollView.frame;
    frame.origin.x = CGRectGetWidth(self.view.frame) * page; // set x
    frame.origin.y = -40;
    frame.size.height += 40;
    view.frame = frame;
}

/****************************************************\
            Logo
\****************************************************/

-(void)displayLogo:(BOOL)visible {
    if (visible) {
        [self.logoImage setHidden:NO];
        [self.logoImage setAlpha:1.0];
        
        [self.dateLabel setAlpha:0.0];
        [self.dateLabel setHidden:YES];
    } else {
        [self.logoImage setAlpha:0.0];
        [self.logoImage setHidden:YES];
        
        [self.dateLabel setHidden:NO];
        [self.dateLabel setAlpha:1.0];
    }
}

/****************************************************\
            Menu toggle
\****************************************************/

- (IBAction)showMenu:(id)sender {
    if (self.menuViewController == nil) {
        self.menuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MenuView"];
    }
    [self addChildViewController:self.menuViewController];
    
    [self.view addSubview:self.menuViewController.view];
    
    [self.menuViewController.view setTransform:CGAffineTransformMakeTranslation(0, -(self.menuViewController.view.frame.size.height))];
    
    [UIView animateWithDuration:0.8
                          delay:0.0
         usingSpringWithDamping:0.8
          initialSpringVelocity:2
                        options:UIViewAnimationOptionCurveEaseInOut //UIViewAnimationOptionTransitionFlipFromTop //UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self.menuViewController.view setTransform:CGAffineTransformMakeTranslation(0,0)];
                     }
                     completion:^(BOOL finished){
                         NSLog(@"COMPLETE ANIM");
                     }];
}

- (void)initProgressSpeech {
    
    self.progressSpeech.backgroundColor = [UIColor clearColor];
    self.progressSpeech.hidden = YES;
    
    int radius = 12;
    self.circle = [CAShapeLayer layer];
    // Make a circular shape
    self.circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0 * radius, 2.0 * radius)
                                             cornerRadius:radius].CGPath;
    // Center the shape in self.view
//    circle.position = CGPointMake(CGRectGetMidX(self.progressSpeech.frame)-radius,
//                                  CGRectGetMidY(self.progressSpeech.frame)-radius);

    self.circle.position = CGPointMake(8, 9);
    
    // Configure the apperence of the circle
    self.circle.fillColor = [UIColor clearColor].CGColor;
    self.circle.strokeColor = [UIColor whiteColor].CGColor;
    self.circle.lineWidth = 1;
    
    // Add to parent layer
    [self.progressSpeech.layer addSublayer:self.circle];
}

- (void)startPlayLayerFrom:(CGFloat)from to:(CGFloat)to duration:(CGFloat)duration {
    // Configure animation
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = duration; // "animate over 10 seconds or so.."
    drawAnimation.repeatCount         = 1.0;  // Animate only once..
    
    // Animate from no part of the stroke being drawn to the entire stroke being drawn
    drawAnimation.fromValue = [NSNumber numberWithFloat:from];
    drawAnimation.toValue   = [NSNumber numberWithFloat:to];
    
    // Experiment with timing to get the appearence to look the way you want
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    // Add the animation to the circle
    [self.circle addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
    
    self.progressSpeech.hidden = NO;
}

- (void)pauseLayer
{
    CFTimeInterval pausedTime = [self.circle convertTime:CACurrentMediaTime() fromLayer:nil];
    self.circle.speed = 0.0;
    self.circle.timeOffset = pausedTime;
    
    // self.manager.currentArticlePlaying.currentProgressLayerTopBar =
}

- (void)resumeLayer
{
    CFTimeInterval pausedTime = [self.circle timeOffset];
    self.circle.speed = 1.0;
    self.circle.timeOffset = 0.0;
    self.circle.beginTime = 0.0;
    CFTimeInterval timeSincePause = [self.circle convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    self.circle.beginTime = timeSincePause;
}

- (void)clearLayer
{
    self.circle.speed = 1.0;
    self.circle.timeOffset = 0.0;
    self.circle.beginTime = 0.0;
    
    self.progressSpeech.hidden = YES;
}



@end
