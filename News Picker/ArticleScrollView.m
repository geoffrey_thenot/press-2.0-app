//
//  ArticleScrollView.m
//  News Picker
//
//  Created by Guillaume Jasmin on 23/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "ArticleScrollView.h"
#import "ManagerData.h"

#define CONSTRAINT_BOTTOM_ARTICLE_TITLE 40
#define CONSTRAINT_TOP_SMALL_ARTICLE_TITLE 0

#define OFFSET_TOP_IMAGE_CONTAINER 250
#define OFFSET_TOP_START_BTN 170
#define OFFSET_TOP_START_SHOW_SMALL_TITLE 230
#define OFFSET_TOP_START_MOVE_TITLE 170


@interface ArticleScrollView ()

@property (strong, nonatomic) ManagerData *manager;

@end

@implementation ArticleScrollView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    // NSLog(@"zzzz");
    
    self.delegate = self;
    
    self.manager = [ManagerData sharedManager];
}

// fonction affine f(x) = ((x - k) + abs(x - k)) / 2
- (CGFloat)affineWithStartConstant:(CGFloat)constant var:(CGFloat)var {
    return ( (var - constant) + sqrt( pow(var - constant, 2) ) ) / 2;
}

// fonction affine f(x) = [((x - k) + abs(x - k)) / 2] - [((x - i) + abs(x - i)) / 2]
- (CGFloat)affineWithStartConstante:(CGFloat)startConstant endConstante:(CGFloat)endConstant var:(CGFloat)var {
    return [self affineWithStartConstant:startConstant var:self.contentOffset.y] - [self affineWithStartConstant:endConstant var:self.contentOffset.y];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    // NSLog(@"Scrolling...");
    // NSLog(@"%f", self.contentOffset.y);
    
    ArticleViewController *controller = self.manager.currentTimeline.articleDetailList.currentArticleViewController;
    
    controller.imageContainerConstraintTop.constant = [self affineWithStartConstant:OFFSET_TOP_IMAGE_CONTAINER var:self.contentOffset.y];
    
    controller.playBtnConstraintBottom.constant = 20 - [self affineWithStartConstante:OFFSET_TOP_START_BTN endConstante:OFFSET_TOP_IMAGE_CONTAINER var:self.contentOffset.y] / 3.5;
    
    controller.closeBtnConstraintBottom.constant = controller.playBtnConstraintBottom.constant;
    
    controller.articleTitleConstraintBottom.constant = CONSTRAINT_BOTTOM_ARTICLE_TITLE + [self affineWithStartConstante:OFFSET_TOP_START_BTN endConstante:OFFSET_TOP_IMAGE_CONTAINER var:self.contentOffset.y] / 1.5;
    
    controller.smallArticleTitleConstraintTop.constant = CONSTRAINT_TOP_SMALL_ARTICLE_TITLE - [self affineWithStartConstante:OFFSET_TOP_START_MOVE_TITLE endConstante:OFFSET_TOP_IMAGE_CONTAINER var:self.contentOffset.y] / 1;
    
    controller.articleTitleSmall.alpha = [self affineWithStartConstante:OFFSET_TOP_START_SHOW_SMALL_TITLE endConstante:OFFSET_TOP_IMAGE_CONTAINER var:self.contentOffset.y] / (OFFSET_TOP_IMAGE_CONTAINER - OFFSET_TOP_START_SHOW_SMALL_TITLE);
    
    controller.articleTitle.alpha = 1 - [self affineWithStartConstante:OFFSET_TOP_START_MOVE_TITLE endConstante:OFFSET_TOP_IMAGE_CONTAINER var:self.contentOffset.y] / (OFFSET_TOP_IMAGE_CONTAINER - OFFSET_TOP_START_MOVE_TITLE);
    
    [controller.view layoutIfNeeded];
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    NSLog(@"scrollViewDidEndDecelerating");
}


@end
