//
//  MainViewController.h
//  News Picker
//
//  Created by geoffrey thenot on 11/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "ViewController.h"

typedef enum {
    CurrentArticleContextTimeline,
    CurrentArticleContextDetail
} CurrentArticleContext;

@interface MainViewController : ViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (assign, nonatomic) CurrentArticleContext currentArticleContext;
@property (strong, nonatomic) IBOutlet UIView *progressSpeech;
@property (strong, nonatomic) CAShapeLayer *circle;

- (IBAction)showMenu:(id)sender;
- (void)updatePlayButton;
- (void)startPlayLayerFrom:(CGFloat)from to:(CGFloat)to duration:(CGFloat)duration;
- (void)pauseLayer;
- (void)resumeLayer;
- (void)clearLayer;


@end
