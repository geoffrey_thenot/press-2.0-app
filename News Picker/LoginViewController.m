//
//  LoginViewController.m
//  News Picker
//
//  Created by THENOT Geoffrey on 07/01/2015.
//  Copyright (c) 2015 THENOT Geoffrey. All rights reserved.
//

#import "LoginViewController.h"
#import "SuperParseTwitter.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    NSLog(@"view didload");
    [super viewDidLoad];
    [[self navigationController] setNavigationBarHidden:NO animated:YES];
    
    [self.usernameField setDelegate:self];
    [self.passwordField setDelegate:self];
    // Do any additional setup after loading the view.
}


- (IBAction)login:(id)sender {
    NSString *username = [self.usernameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString *password = [self.passwordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([username length] == 0 || [password length] == 0) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Oops!" message:@"Please enter a Username and Password!"delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alertView show];
    } else {
        [PFUser logInWithUsernameInBackground:username password:password block:^(PFUser *user, NSError *error) {
            if (!error) {
                [self.navigationController popToRootViewControllerAnimated:YES];
            } else {
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:[error userInfo][@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alertView show];
            }
        }];
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.usernameField) {
        [textField resignFirstResponder];
        [self.passwordField becomeFirstResponder];
    }
    else if (textField == self.passwordField) {
        [self login:nil];
    }
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
    [super touchesBegan:touches withEvent:event];
}


- (IBAction)facebookLogin:(id)sender {
    NSArray *permissionsArray = @[ @"user_about_me", @"user_friends", @"user_groups", @"email"];
    [PFFacebookUtils logInWithPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        if (!user) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:[error userInfo][@"error"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
        } else {
            [FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                if (!error) {
                    // Store the current user's Facebook ID on the user
                    [[PFUser currentUser] setObject:[result objectForKey:@"id"]
                                             forKey:@"fbId"];
                    
                    [[PFUser currentUser] setObject:[result objectForKey:@"email"]
                                             forKey:@"fbEmail"];
                    
                    [[PFUser currentUser] setObject:[result objectForKey:@"name"]
                                             forKey:@"fbName"];
                    
                    [[PFUser currentUser] saveInBackground];
                }
            }];
            if (user.isNew) {
                [self.delegate loginSuccess];
            } else {
                [self.delegate loginSuccess];
            }
        }
    }];
}

- (IBAction)twitterLogin:(id)sender {
    [SuperParseTwitter logInWithView:self.view Block:^(PFUser *user, NSError *error) {
        if (!user) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry!" message:@"Twitter wasn't enabled in your settings" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alertView show];
            return;
        } else if (user.isNew) {
            [self.delegate loginSuccess];
        } else {
            [self.delegate loginSuccess];
        }
    }];
}

- (IBAction)goToSignup:(id)sender {
    [self.delegate subscribeClick];
}

@end
