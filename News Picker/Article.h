//
//  Article.h
//  News Picker
//
//  Created by Guillaume Jasmin on 30/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <Parse/Parse.h>

@class TimelineItem;
@class ArticleViewController;

typedef enum {
    PlayerIconStatePlay,
    PlayerIconStatePlayToPause,
    PlayerIconStatePauseToPlay,
    PlayerIconStatePauseToReplay,
    PlayerIconStateReplayToPause,
    PlayerIconStatePause,
    PlayerIconStateReplay
} PlayerIconState;

typedef enum {
    PlayerStateToPlay,
    PlayerStateToContinueFromNewUtterance,
    PlayerStateToContinueFromExistingUtterance,
    PlayerStateToPause,
    PlayerStateToReplay
} PlayerState;

typedef enum {
    CursorStateToPlay,
    CursorStateToContinueFromNewLayer,
    CursorStateToContinueFromExistingLayer,
    CursorStateToReplay
} CursorState;



@interface Article : NSObject

- (id)initWithPFArticle:(PFObject *)PFArticle;
- (void)setupImage;
- (void)createMergeImage;
- (void)createGradiantMapFilter;
- (void)endPlaying;
- (void)loadUtterance;

- (void)onClickPlayBtnFromTimeline:(TimelineItem *)timelineItem setArticleDetail:(ArticleViewController *)articleDetail;
- (void)animFilter:(TimelineItem *)timelineItem setArticleDetail:(ArticleViewController *)articleDetail;
- (void)pauseActionFromTimeline:(TimelineItem *)timelineItem setArticleDetail:(ArticleViewController *)articleDetail updateIcon:(BOOL)updateIcon;

// speech
- (void)pauseLayer:(CALayer *)layer;
- (void)resumeLayer:(CALayer *)layer;
- (void)clearLayer:(CALayer *)layer;

- (void)updateIcon:(UIButton *)playBtn setSuffixState:(NSString *)suffixState setSuffixStatePause:(NSString *)suffixStatePause setAnimatedState:(NSString *)animatedState;


@property (strong, nonatomic) PFObject *PFArticle;

// typedef
@property (assign, nonatomic) PlayerIconState playerIconState;
@property (assign, nonatomic) PlayerState     playerState;
@property (assign, nonatomic) CursorState     cursorState;

@property (strong, nonatomic) TimelineItem *timelineItem;
@property (strong, nonatomic) ArticleViewController *articleViewController;

@property (strong, nonatomic) NSString *category;
@property (strong, nonatomic) NSData   *imageData;
@property (strong, nonatomic) UIImage  *image;
@property (strong, nonatomic) UIImage  *imageFiltered;
@property (assign, nonatomic) BOOL     isHighlighted;
@property (assign, nonatomic) BOOL     isPlaying;

// player sound
@property (strong, nonatomic) AVSpeechUtterance *utterance;
@property (strong, nonatomic) NSString  *currentVoiceString;
@property (assign, nonatomic) NSInteger characterRangeLocationRemaining;

// timer
@property (readwrite, nonatomic) CGFloat startTimePlay;
@property (readwrite, nonatomic) CGFloat offsetTimePlay;
@property (readwrite, nonatomic) CGFloat totalTimePlay;

// mask


@property (assign, nonatomic) CGFloat maskStartPointY;
@property (assign, nonatomic) CGFloat smallMaskStartPointY;
@property (assign, nonatomic) CGFloat maskHeight;
@property (assign, nonatomic) CGFloat smallMaskHeight;
@property (assign, nonatomic) CGFloat maskMarginArc;
@property (assign, nonatomic) CGFloat maskWidth;
@property (assign, nonatomic) CGFloat maskStartPointX;
@property (assign, nonatomic) CGFloat maskStartPointXOrigin;
@property (assign, nonatomic) CGFloat starMaskAnimationPointX;
@property (assign, nonatomic) CGFloat starMaskAnimationPointXOffset;


@property (strong, nonatomic) UIButton *playBtn;
@property (strong, nonatomic) CABasicAnimation *anim;

@property (assign, nonatomic) CGFloat maskLayerPosXOffset;
@property (assign, nonatomic) CGFloat maskLayerPosX;

@property (strong, nonatomic) CALayer *maskLayerSmall;
@property (strong, nonatomic) CALayer *maskLayerLarge;

@property (assign, nonatomic) BOOL isNewProgressLayerTopBar;

@property (assign, nonatomic) BOOL isHighlightedByPeople;


@end
