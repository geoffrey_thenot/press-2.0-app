//
//  Article.m
//  News Picker
//
//  Created by Guillaume Jasmin on 30/12/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "Article.h"
#import "Helper.h"
#import "ManagerData.h"


#define LAST_ICON_PLAY_NUMBER @"00014"
#define LAST_ICON_PAUSE_NUMBER @"00014"
#define LAST_ICON_REPLAY_NUMBER @"00027"

@interface Article ()

@property (assign, nonatomic) BOOL isPauseTimer;
@property (strong, nonatomic) ManagerData *manager;


@end

@implementation Article

- (id)init {
    
    return self;
}

- (id)initWithPFArticle:(PFObject *)PFArticle
{
    
    NSLog(@"initWithPFArticle");
    
    self.PFArticle = PFArticle;
    // NSLog(@"init %@", self.PFArticle[@"title"]);
    
    self.manager = [ManagerData sharedManager];
    
    [self setupMaskProperties];
    [self createMaskLayer];
    
    self.totalTimePlay = (CGFloat)[self.PFArticle[@"title"] length] / 15;
    self.characterRangeLocationRemaining = [self.PFArticle[@"title"] length];
    
    // flag
    self.maskLayerPosXOffset = 0;
    self.maskLayerPosX = 0;
    self.isNewProgressLayerTopBar = YES;
    
    return self;
}


- (void)setupImage {
    [self createGradiantMapFilter];
    [self createMergeImage];
}

- (void)createGradiantMapFilter {
    CIImage   *imageFilter   = [CIImage imageWithData:self.imageData];
    CIContext *context       = [CIContext contextWithOptions:nil];
    CIImage   *gradiantImage = [[CIImage alloc] initWithImage:[UIImage imageNamed:@"timeline-filtre"]];
    CIFilter  *imgFilter     = [CIFilter filterWithName:@"CIColorMap"];
    
    [imgFilter setValue:imageFilter forKey:@"inputImage"];
    [imgFilter setValue:gradiantImage forKey:@"inputGradientImage"];
    
    CIImage *output          = [imgFilter valueForKey: @"outputImage"];
    CGImageRef cgImgRef      = [context createCGImage:output fromRect:[output extent]];
    UIImage *imageWithFilter = [UIImage imageWithCGImage:cgImgRef];
    CGImageRelease(cgImgRef);
    
    self.imageFiltered = imageWithFilter;
}

- (void)setupMaskProperties
{
    // common properties
    self.maskMarginArc           = 25;
    self.maskWidth               = [[UIScreen mainScreen] bounds].size.width;
    self.maskStartPointXOrigin   = - self.maskWidth - self.maskMarginArc;
    self.maskStartPointX         = self.maskStartPointXOrigin;
    self.starMaskAnimationPointX = 0;
    
    //----- custom properties
    // large mask
    self.maskHeight              = 360;
    self.maskStartPointY         = -20;
    
    // small mask
    self.smallMaskStartPointY    = 0;
    self.smallMaskHeight         = 115;
}


- (void)createMergeImage {
    
    
    CIContext *coreImageContext = [CIContext contextWithOptions:nil];
    CIFilter *gradientFilter = [CIFilter filterWithName:@"CILinearGradient"];
    
    [gradientFilter setDefaults];
    
    CIColor *startColor   = [CIColor colorWithCGColor:[UIColor clearColor].CGColor];
    CIColor *endColor     = [CIColor colorWithCGColor:Rgb2UIColorAlpha(0, 0, 0, 0.7).CGColor];
    CIVector *startVector = [CIVector vectorWithX:0 Y:200];
    CIVector *endVector   = [CIVector vectorWithX:0 Y:0];
    
    [gradientFilter setValue:startVector forKey:@"inputPoint0"];
    [gradientFilter setValue:endVector   forKey:@"inputPoint1"];
    [gradientFilter setValue:startColor  forKey:@"inputColor0"];
    [gradientFilter setValue:endColor    forKey:@"inputColor1"];
    
    CIImage *outputImage = [gradientFilter outputImage];
    CGImageRef cgimg = [coreImageContext createCGImage:outputImage fromRect:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 360)];
    
    UIImage *blackImage = [UIImage imageWithCGImage:cgimg];
    
    
    // ---
    
    CGSize newImageSize = CGSizeMake(self.image.size.width, self.image.size.height);
    //if (UIGraphicsBeginImageContextWithOptions != NULL) {
    UIGraphicsBeginImageContextWithOptions(newImageSize, NO, [[UIScreen mainScreen] scale]);
    //} else {
    //    UIGraphicsBeginImageContext(newImageSize);
    //}
    
    [self.image drawAtPoint:CGPointMake(roundf((newImageSize.width - self.image.size.width) / 2),
                                                roundf((newImageSize.height - self.image.size.height) / 2))];
    
    [blackImage drawAtPoint:CGPointMake(roundf((newImageSize.width - blackImage.size.width) / 2),
                                        roundf((newImageSize.height - blackImage.size.height) / 2))];
    
    UIImage *imageFinal = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.image = imageFinal;
}

- (void)endPlaying
{
    self.playerIconState = PlayerIconStatePauseToReplay;
    self.playerState = PlayerStateToReplay;
    self.cursorState = CursorStateToReplay;
    
    // self.characterRangeLocationRemaining = -2;
    self.characterRangeLocationRemaining = [self.PFArticle[@"title"] length];
    self.maskLayerPosXOffset = 0;

    self.isPlaying = NO;
    
    NSLog(@"dddd --- %@", self.PFArticle[@"title"]);
    
    if (self.manager.mainViewController.currentArticleContext == CurrentArticleContextTimeline) {
        [self.timelineItem updateIcon];
        if (self.manager.currentTimeline.playInContinue) {
            [self.manager.currentTimeline playNextArticle];
        }
    }
    else if(self.manager.mainViewController.currentArticleContext == CurrentArticleContextDetail) {
        [self.articleViewController updateIcon];
    }
    
}


- (void)loadUtterance {
    // load utterance
    
    if(self.playerState == PlayerStateToPause){
        return;
    }
    
    if(self.characterRangeLocationRemaining > 0) {
        self.currentVoiceString = [self.PFArticle[@"title"] substringFromIndex:[self.PFArticle[@"title"] length] - self.characterRangeLocationRemaining];
    }
    else {
        self.currentVoiceString = self.PFArticle[@"title"];
    }
    
    self.utterance = [[AVSpeechUtterance alloc] initWithString:self.currentVoiceString];
    self.utterance.rate = 0.05;
    
    NSString *language = @"en-au";
    // NSString *language = @"en-GB";
    self.utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:language];
    
}


// ----------------------------------------------------------------------
//                             Pause speech
// ----------------------------------------------------------------------
- (void)pauseLayer:(CALayer *)layer
{
    CFTimeInterval pausedTime = [layer convertTime:CACurrentMediaTime() fromLayer:nil];
    layer.speed = 0.0;
    layer.timeOffset = pausedTime;
}

// ----------------------------------------------------------------------
//                             Continue speech
// ----------------------------------------------------------------------
- (void)resumeLayer:(CALayer *)layer
{
    CFTimeInterval pausedTime = [layer timeOffset];
    layer.speed = 1.0;
    layer.timeOffset = 0.0;
    layer.beginTime = 0.0;
    CFTimeInterval timeSincePause = [layer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    layer.beginTime = timeSincePause;
}

- (void)clearLayer:(CALayer *)layer
{
    layer.speed = 1.0;
    layer.timeOffset = 0.0;
    layer.beginTime = 0.0;
}

- (void)createMaskLayer
{
    NSLog(@"createMaskLayer");
    
    CGFloat maskStartPointX = self.maskStartPointX;
    CGFloat maskWidth       = self.maskWidth;
    CGFloat maskMarginArc   = self.maskMarginArc;
    
    CGFloat maskHeight;
    CGFloat maskStartPointY;

    // -- small
    maskHeight      = self.smallMaskHeight;
    maskStartPointY = self.smallMaskStartPointY;
    
    // small mask layer
    CAShapeLayer *maskLayerSmall = [CAShapeLayer layer];
    UIBezierPath *filterPathSmall = [UIBezierPath bezierPath];
    [filterPathSmall moveToPoint:CGPointMake(maskStartPointX, maskStartPointY)];
    [filterPathSmall addLineToPoint:CGPointMake(maskStartPointX + maskWidth, maskStartPointY)];
    [filterPathSmall addCurveToPoint:CGPointMake(maskStartPointX + maskWidth, maskHeight)
                  controlPoint1:CGPointMake(maskStartPointX + maskWidth + maskMarginArc, maskHeight / 2)
                  controlPoint2:CGPointMake(maskStartPointX + maskWidth, maskHeight)];
    [filterPathSmall addLineToPoint:CGPointMake(maskStartPointX + maskWidth, maskHeight)];
    [filterPathSmall addLineToPoint:CGPointMake(maskStartPointX, maskHeight)];
    [filterPathSmall closePath];
    maskLayerSmall.backgroundColor = [[UIColor clearColor] CGColor];
    maskLayerSmall.path = [filterPathSmall CGPath];
    self.maskLayerSmall = maskLayerSmall;
    
    // -- small
    maskHeight      = self.maskHeight;
    maskStartPointY = self.maskStartPointY;
    
    // large mask layer
    CAShapeLayer *maskLayerLarge = [CAShapeLayer layer];
    UIBezierPath *filterPathLarge = [UIBezierPath bezierPath];
    [filterPathLarge moveToPoint:CGPointMake(maskStartPointX, maskStartPointY)];
    [filterPathLarge addLineToPoint:CGPointMake(maskStartPointX + maskWidth, maskStartPointY)];
    [filterPathLarge addCurveToPoint:CGPointMake(maskStartPointX + maskWidth, maskHeight)
                  controlPoint1:CGPointMake(maskStartPointX + maskWidth + maskMarginArc, maskHeight / 2)
                  controlPoint2:CGPointMake(maskStartPointX + maskWidth, maskHeight)];
    [filterPathLarge addLineToPoint:CGPointMake(maskStartPointX + maskWidth, maskHeight)];
    [filterPathLarge addLineToPoint:CGPointMake(maskStartPointX, maskHeight)];
    [filterPathLarge closePath];
    maskLayerLarge.backgroundColor = [[UIColor clearColor] CGColor];
    maskLayerLarge.path = [filterPathLarge CGPath];
    self.maskLayerLarge = maskLayerLarge;
}

- (void)updateIcon:(UIButton *)playBtn setSuffixState:(NSString *)suffixState setSuffixStatePause:(NSString *)suffixStatePause setAnimatedState:(NSString *)animatedState {
    
    // NSLog(@"Update icon");
    
    // NSLog(@"Update icon %@", self.PFArticle[@"title"]);
    
//    NSLog(@"suffixState %@", suffixState);
//    NSLog(@"suffixStatePause %@", suffixStatePause);
//    NSLog(@"animatedState %@", animatedState);
    
    NSString *imageName;
    UIImage *image;
    NSString *animatedImagesName;
    
    switch (self.playerIconState) {
        case PlayerIconStatePlayToPause:
            
            // NSLog(@"PlayerIconStatePlayToPause");
            
            animatedImagesName = [NSString stringWithFormat:@"animation-play-pause-%@-state_", animatedState];
            
            playBtn.imageView.animationImages = [Helper createAnimationArray:animatedImagesName imgCout:14];
            playBtn.imageView.animationDuration = 0.5;
            playBtn.imageView.animationRepeatCount = 1;
            imageName = [NSString stringWithFormat:@"%@00014", animatedImagesName];
            image = [UIImage imageNamed:imageName];
            [playBtn setImage:image forState:UIControlStateNormal];
            [playBtn.imageView startAnimating];
            
            self.playerIconState = PlayerIconStatePause;
            
            break;
            
            
        case PlayerIconStatePauseToPlay:
            
            // NSLog(@"PlayerIconStatePauseToPlay");
            
            animatedImagesName = [NSString stringWithFormat:@"animation-pause-play-%@-state_", animatedState];
            
            playBtn.imageView.animationImages = [Helper createAnimationArray:animatedImagesName imgCout:14];
            playBtn.imageView.animationDuration = 0.5;
            playBtn.imageView.animationRepeatCount = 1;
            imageName = [NSString stringWithFormat:@"%@00014", animatedImagesName];
            image = [UIImage imageNamed:imageName];
            [playBtn setImage:image forState:UIControlStateNormal];
            [playBtn.imageView startAnimating];
            
            self.playerIconState = PlayerIconStatePlay;
            
            break;
            
        case PlayerIconStatePauseToReplay:
            
            // NSLog(@"PlayerIconStatePauseToReplay");
            
            animatedImagesName = [NSString stringWithFormat:@"animation-pause-replay-%@-state_", animatedState];
            
            playBtn.imageView.animationImages = [Helper createAnimationArray:animatedImagesName imgCout:27];
            playBtn.imageView.animationDuration = 0.7;
            playBtn.imageView.animationRepeatCount = 1;
            imageName = [NSString stringWithFormat:@"%@00027", animatedImagesName];
            image = [UIImage imageNamed:imageName];
            [playBtn setImage:image forState:UIControlStateNormal];
            [playBtn.imageView startAnimating];
            
            self.playerIconState = PlayerIconStateReplay;
            
            break;
            
        case PlayerIconStateReplayToPause:
            
            // NSLog(@"PlayerIconStateReplayToPause");
            
            animatedImagesName = [NSString stringWithFormat:@"animation-replay-pause-%@-state_", animatedState];
            
            playBtn.imageView.animationImages = [Helper createAnimationArray:animatedImagesName imgCout:27];
            playBtn.imageView.animationDuration = 0.7;
            playBtn.imageView.animationRepeatCount = 1;
            imageName = [NSString stringWithFormat:@"%@00027", animatedImagesName];
            image = [UIImage imageNamed:imageName];
            [playBtn setImage:image forState:UIControlStateNormal];
            [playBtn.imageView startAnimating];
            
            self.playerIconState = PlayerIconStatePause;
            
            break;
            
        case PlayerIconStatePause:
            
            NSLog(@"PlayerIconStatePause");
            
            imageName = [NSString stringWithFormat:@"animation-pause-play-%@-state_00000", animatedState];
            image = [UIImage imageNamed:imageName];
            [playBtn setImage:image forState:UIControlStateNormal];
            
            break;
            
        case PlayerIconStatePlay:
            
            // NSLog(@"PlayerIconStatePlay");
            
            imageName = [NSString stringWithFormat:@"animation-pause-play-%@-state_%@", animatedState, LAST_ICON_PLAY_NUMBER];
            image = [UIImage imageNamed:imageName];
            [playBtn setImage:image forState:UIControlStateNormal];
            
            break;
            
        case PlayerIconStateReplay:
            
            // NSLog(@"PlayerIconStateReplay");
            
            imageName = [NSString stringWithFormat:@"animation-pause-replay-%@-state_%@", animatedState, LAST_ICON_REPLAY_NUMBER];
            image = [UIImage imageNamed:imageName];
            [playBtn setImage:image forState:UIControlStateNormal];
            
            break;
            
        default:
            NSLog(@"ERROR");
            break;
    }
    
    NSLog(@"animatedImagesName %@", animatedImagesName);
    NSLog(@"imageName %@", animatedImagesName);
    
}


- (void)onClickPlayBtnFromTimeline:(TimelineItem *)timelineItem setArticleDetail:(ArticleViewController *)articleDetail
{
    
    self.manager.currentArticle = self;

    if(self.playerState != PlayerStateToPause){
        if(self.imageFiltered == nil){
            [self createGradiantMapFilter];
        }
        [self playActionFromTimeline:timelineItem setArticleDetail:articleDetail];
    }
    
    // pause action
    else {
        [self pauseActionFromTimeline:timelineItem setArticleDetail:articleDetail updateIcon:YES];
    }
}

- (void)playActionFromTimeline:(TimelineItem *)timelineItem setArticleDetail:(ArticleViewController *)articleDetail {
    
    // NSLog(@"play action");
    
    self.playerIconState = PlayerIconStatePlayToPause;
    
    if (timelineItem != nil) {
        // if an other article is playing
        if(self.manager.synthesizer.currentTimelineItemDataPlay && self.manager.synthesizer.currentTimelineItemDataPlay.article.playerState == PlayerStateToPause) {
            [self.manager.synthesizer.currentTimelineItemPlay pauseActionWithUpdateIcon:NO];
            // [self.manager.currentArticle pauseActionFromTimeline:timelineItem setArticleDetail:articleDetail updateIcon:NO];
            
            // if cell which is still playing is not a visible cell
            self.manager.synthesizer.currentTimelineItemDataPlay.article.playerState = PlayerStateToContinueFromExistingUtterance;
        }
        
        if(self.manager.synthesizer.currentTimelineItemPlay != timelineItem) {
            self.playerState = PlayerStateToContinueFromNewUtterance;
        }
        
        self.manager.synthesizer.currentTimelineItemPlay     = timelineItem;
        self.manager.synthesizer.currentTimelineItemDataPlay = timelineItem.data;
    }
    
    // self.manager.currentArticlePlaying = self;
    // self.manager.currentArticle = self;
    
    if (self.manager.currentArticlePlaying == self) {
        [self.manager.mainViewController resumeLayer];
    }
    else {
        CGFloat from = 1 - (CGFloat)(self.characterRangeLocationRemaining / (CGFloat)[self.currentVoiceString length]);
        
        // NSLog(@"from %f", from);
        CGFloat to = 1;
        CGFloat duration = self.characterRangeLocationRemaining * self.totalTimePlay / [self.currentVoiceString length];
        
        // main anim layer
        [self.manager.mainViewController clearLayer];
        [self.manager.mainViewController startPlayLayerFrom:from to:to duration:duration];
    }
    
    
    self.manager.currentArticlePlaying = self;
    
    // first play
    if(self.playerState != PlayerStateToContinueFromExistingUtterance) {
        
        // NSLog(@"first play");
        
        [self.manager.synthesizer stopSpeakingAtBoundary:NO];
        [self loadUtterance];
        [self.manager.synthesizer speakUtterance:self.utterance];
        
    }
    
    // continue
    else {
        // NSLog(@"continue");
        [self.manager.synthesizer continueSpeaking];
    }
    
    // --------- cursor
    
    switch (self.cursorState) {
        case CursorStateToPlay:
            [self animFilter:timelineItem setArticleDetail:articleDetail];
            
            break;
        
        case CursorStateToContinueFromExistingLayer:
            [self resumeLayer:self.maskLayerSmall];
            [self resumeLayer:self.maskLayerLarge];
            
            
            break;
            
        case CursorStateToReplay:
            [self clearLayer:self.maskLayerSmall];
            [self clearLayer:self.maskLayerLarge];
            [self animFilter:timelineItem setArticleDetail:articleDetail];
            
            self.playerIconState = PlayerIconStateReplayToPause;
            
            break;
            
        default:
            break;
    }
    
    
    // --------- end cursor
    
    self.playerState = PlayerStateToPause;
    self.isPlaying = YES;
    
    if (timelineItem != nil) {
        [timelineItem callbackPlay];
    }
    else {
        [articleDetail callbackPlay];
    }
    
    self.playerIconState = PlayerIconStatePause;
    
}

- (void)pauseActionFromTimeline:(TimelineItem *)timelineItem setArticleDetail:(ArticleViewController *)articleDetail updateIcon:(BOOL)updateIcon {
    // NSLog(@"pause");
    
    [self.manager.synthesizer pauseSpeakingAtBoundary:NO];
    
    [self pauseLayer:self.maskLayerSmall];
    [self pauseLayer:self.maskLayerLarge];
    
    self.playerState = PlayerStateToContinueFromExistingUtterance;
    self.cursorState = CursorStateToContinueFromExistingLayer;
    
    self.playerIconState = PlayerIconStatePauseToPlay;

    self.isPlaying = NO;
    
    if (updateIcon) {
        if (timelineItem != nil){
            [timelineItem updateIcon];
        }
        else {
            [articleDetail updateIcon];
        }
    }
    
    [self.manager.mainViewController pauseLayer];
    
}


- (void)animFilter:(TimelineItem *)timelineItem setArticleDetail:(ArticleViewController *)articleDetail {
    
    // NSLog(@"animFilter");
    
    self.anim = [CABasicAnimation new];
    self.anim.removedOnCompletion = NO;
    self.anim.fillMode = kCAFillModeForwards;
    
    CGFloat startPosX = self.maskLayerPosX;
    CGFloat endPosX = self.maskWidth + self.maskMarginArc - self.maskLayerPosXOffset;
    
    // NSLog(@"startPosX %f", startPosX);
    
    self.anim.fromValue = [NSValue valueWithCGPoint:CGPointMake(startPosX, 0)];
    self.anim.toValue = [NSValue valueWithCGPoint:CGPointMake(endPosX, 0)];
    
    self.anim.keyPath = @"position";
    self.anim.timingFunction = [CAMediaTimingFunction functionWithName:@"linear"];
    
    self.anim.duration = self.characterRangeLocationRemaining * self.totalTimePlay / [self.currentVoiceString length];
    

    [self.maskLayerSmall addAnimation:self.anim forKey:@"position"];
    [self.maskLayerLarge addAnimation:self.anim forKey:@"position"];
    
}



@end
