//
//  Helper.m
//  News Picker
//
//  Created by Guillaume Jasmin on 09/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "Helper.h"

@interface Helper ()

@end

@implementation Helper


+ (NSArray *)createAnimationArray:(NSString *)name imgCout:(NSInteger)imgCount {
    
    NSMutableArray *imagesList = [NSMutableArray new];
    
    for(int i = 0; i < imgCount + 1; i++) {
        
        NSString *prefixNumber;
        
        if (i < 10) {
            prefixNumber = @"0000";
        }
        else if (i < 100) {
            prefixNumber = @"000";
        }
        else if (i < 1000) {
            prefixNumber = @"00";
        }
        else if (i < 10000) {
            prefixNumber = @"0";
        }
        
        //if(i == imgCount + 1) {
        //    [imagesList addObject:nil];
        //}
        //else {
            NSString *fileName = [NSString stringWithFormat:@"%@%@%d", name, prefixNumber, i];
            [imagesList addObject:[UIImage imageNamed:fileName]];
        //}
        
    }
    
    return [[NSArray alloc] initWithArray:imagesList];
}

//- (CGRect)autoSizeWithFrame:(CGRect)frame marginLeft:(CGFloat)marginLeft marginRight:(CGFloat)marginRight {
//
//    CGFloat objectMarginLeft  = marginLeft;
//    CGFloat objectMarginRight = marginRight;
//    CGFloat objectWidth       = [[UIScreen mainScreen] bounds].size.width - objectMarginLeft - objectMarginRight;
//    
//    CGRect objectFrame = CGRectMake(objectMarginLeft, frame.origin.y, objectWidth, frame.size.height);
//    
//    NSLog(@"%f", objectFrame.size.width);
//    
//    return objectFrame;
//}

@end
