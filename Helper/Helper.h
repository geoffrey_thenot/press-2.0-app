//
//  Helper.h
//  News Picker
//
//  Created by Guillaume Jasmin on 09/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NPColor.h"

#import "PullableView.h"

// color
#define Rgb2UIColor(r, g, b) [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define Rgb2UIColorAlpha(r, g, b, a) [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:(a)]

// device size
#define isIphone5     ([[UIScreen mainScreen] bounds].size.height == 568) ? TRUE : FALSE
#define isIphone6     ([[UIScreen mainScreen] bounds].size.height == 667) ? TRUE : FALSE
#define isIphone6Plus ([[UIScreen mainScreen] bounds].size.height == 736) ? TRUE : FALSE

#define iphone5Width 320
#define iphone6Width 375

#define DEGREES_TO_RADIANS(angle) ((angle) / 180.0 * M_PI)
#define RADIANS_TO_DEGREES(angle) ((angle) * 180.0 / M_PI)

@interface Helper : NSObject

// - (CGRect)autoSizeWithFrame:(CGRect)frame marginLeft:(CGFloat)marginLeft marginRight:(CGFloat)marginRight;

+ (NSArray *)createAnimationArray:(NSString *)name imgCout:(NSInteger)imgCount;

@end
