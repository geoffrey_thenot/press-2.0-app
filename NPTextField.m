//
//  NPTextField.m
//  News Picker
//
//  Created by THENOT Geoffrey on 05/01/2015.
//  Copyright (c) 2015 THENOT Geoffrey. All rights reserved.
//

#import "NPTextField.h"

@implementation NPTextField


- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectMake(bounds.origin.x + 15, bounds.origin.y,
                      bounds.size.width - 30, bounds.size.height);
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [self textRectForBounds:bounds];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
