//
//  NPColor.h
//  News Picker
//
//  Created by geoffrey thenot on 11/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Extensions)

+ (UIColor *)NPYellow;
+ (UIColor *)NPYellowTranslucid;
+ (UIColor *)NPDarkGray;
+ (UIColor *)NPDarkGrayTranslucid;
+ (UIColor *)NPGray;
+ (UIColor *)NPLightGray;
+ (UIColor *)NPWhite;
@end
