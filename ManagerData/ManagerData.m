//
//  Manager.m
//  News Picker
//
//  Created by Guillaume Jasmin on 09/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "ManagerData.h"


@implementation ManagerData

#pragma mark Singleton Methods

+ (id)sharedManager {
    
    static ManagerData *manager = nil;
    
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        manager = [[self alloc] init];
        
    });
    
    return manager;
}

- (id)init {
    
    if (self = [super init]) {
        // code...
        
        self.helper = [Helper new];
        
        
        self.synthesizer = [[AVSpeechSynthesizerController alloc] init];
        // self.synthesizer.delegate = self;
        // self.utterances  = [NSMutableArray new];

//        self.voiceIsPlaying = NO;
//        self.voiceIsInPause = NO;
        
        [self listener];
    }
    
    return self;
}

- (void)listener {
    

//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(pauseArticleNotification:)
//                                                 name:@"pauseArticle" object:nil];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(endSpeech:)
//                                                 name:@"endSpeech" object:nil];
}

//- (void)pauseArticleNotification:(NSNotification *)notification {
//    [self.currentTimeline pauseArticleNotification:notification];
//}
//
//- (void)endSpeech:(NSNotification *)notification {
//    [self.currentTimeline endSpeech:notification];
//}


@end
