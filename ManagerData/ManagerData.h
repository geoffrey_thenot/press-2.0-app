//
//  Manager.h
//  News Picker
//
//  Created by Guillaume Jasmin on 09/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import "AVSpeechSynthesizerController.h"
#import "Helper.h"
#import "Timeline.h"
#import "MainViewController.h"
#import "ArticleViewController.h"
#import "ArticleDetailList.h"
#import "Article.h"

@interface ManagerData : NSObject <AVSpeechSynthesizerDelegate>


+ (id)sharedManager;
//- (void)changeSound:(NSString *)string;
//- (void)playUtteranceAtIndex:(NSIndexPath *)indexPath;

@property (strong, nonatomic) Helper *helper;
@property (strong, nonatomic) AVSpeechSynthesizerController *synthesizer;
@property (strong, nonatomic) NSMutableArray                *utterances;

//@property (assign, nonatomic) BOOL voiceIsPlaying;
//@property (assign, nonatomic) BOOL voiceIsInPause;
//@property (assign, nonatomic) NSIndexPath *currentVoiceIndexPath;

// @property (strong, nonatomic) AVSpeechUtterance   *utterance;
// @property (strong, nonatomic) NSString            *currentPlaying;

@property (assign, nonatomic) NSString *currentVoiceTitle;
@property (strong, nonatomic) Timeline *currentTimeline;
@property (strong, nonatomic) MainViewController *mainViewController;
@property (strong, nonatomic) ArticleViewController *articleController;
@property (strong, nonatomic) ArticleDetailList     *articleDetailList;
@property (strong, nonatomic) Article *currentArticle;
@property (strong, nonatomic) Article *currentArticlePlaying;

@property (strong, nonatomic) UIButton *totalCategoriesButton;

@property (assign, nonatomic) BOOL willPlayTimeline;

@end
