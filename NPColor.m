//
//  NPColor.m
//  News Picker
//
//  Created by geoffrey thenot on 11/11/2014.
//  Copyright (c) 2014 THENOT Geoffrey. All rights reserved.
//

#import "NPColor.h"

@implementation UIColor (Extensions)

+ (UIColor *)NPYellow {
    return [UIColor colorWithRed:(255/255.f) green:(240/255.f) blue:(0/255.f) alpha:1];
}

+ (UIColor *)NPYellowTranslucid {
    return [UIColor colorWithRed:(255/255.f) green:(240/255.f) blue:(0/255.f) alpha:0.8];
}

+ (UIColor *)NPDarkGray {
    return [UIColor colorWithRed:(29/255.f) green:(31/255.f) blue:(39/255.f) alpha:1];
}

+ (UIColor *)NPDarkGrayTranslucid {
    return [UIColor colorWithRed:(29/255.f) green:(31/255.f) blue:(39/255.f) alpha:0.8];
}

//+ (UIColor *)NPDarkGray {
//    return [UIColor colorWithRed:(70/255.f) green:(72/255.f) blue:(81/255.f) alpha:1];
//}

+ (UIColor *)NPGray {
    return [UIColor colorWithRed:(160/255.f) green:(163/255.f) blue:(180/255.f) alpha:1];
}
+ (UIColor *)NPLightGray {
    return [UIColor colorWithRed:(237/255.f) green:(241/255.f) blue:(244/255.f) alpha:1];
}
+ (UIColor *)NPWhite {
    return [UIColor colorWithRed:(255/255.f) green:(255/255.f) blue:(255/255.f) alpha:1];
}
@end
