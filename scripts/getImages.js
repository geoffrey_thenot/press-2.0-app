/**

$ node index.js

// download from serve
$ node index.js dl

// download from serve, unzip and build
$ node index.js dl build

// get from local if exist, else, download from server
$ node index.js local
$ node index.js local build

$ node index.js unzip
$ node index.js unzip build

$ node index.js build

*/


var fs = require('fs');
var open = require('open');
// var dbox = require('dbox');
var path = require('path-extra');
var sys = require('sys')
var exec = require('child_process').exec;

// var
var DROPBOX_PATH = '/Dropbox/gobelins-press-2.0/05_Maquettes/images-ios/';
// var DROPBOX_DOWNLOAD_URL = 'https://www.dropbox.com/sh/vg5h08anb0l9fsp/AADvxG_pPaMsPjqyCfpM6dK-a?dl=1';
var DROPBOX_DOWNLOAD_URL = 'https://www.dropbox.com/sh/u5lmu91gl8q303d/AAB9WBc9HHWU_0BDEMPPMO5Ga?dl=1';
var IMG_PATH_SRC = './images-ios/';
// var IMG_PATH_OUTPUT = './../guillaumev2/Images.xcassets/img-app/';
var IMG_PATH_OUTPUT = './../News Picker/Images.xcassets/img-app/';
var FILE_DOWNLOAD_ZIP = path.homedir() + '/Downloads/images-ios.zip';
var IMG_FOLDER_SUFFIX = '.imageset';

var puts = function (error, stdout, stderr) {
    sys.puts(stdout);
}

var init = function () {

    var args = process.argv.slice(2);

    if(!args[0]){
        if(!getFromLocalDropbox()){
            downloadDropboxFile();
            unzipRecurse.init();
        }
        else {
            buildImages();
        }
        return false;
    }

    if(args[0] === 'local'){
        getFromLocalDropbox();
        if(args[1] === 'build'){
            buildImages();
        }
    }

    if(args[0] === 'dl' || args[0] === 'download'){
        downloadDropboxFile();
        if(args[1] === 'build'){
            unzipRecurse.init();
        }
        return false;
    }

    if(args[0] === 'unzip'){
        if(args[1] === 'build'){
            unzipRecurse.init();
        }
        else {
            unzip();
        }

        return false;
    }

    if(args[0] === 'build'){
        buildImages();
    }

};

var getFromLocalDropbox = function () {

    // remove images-ios folder in NodeJS projet
    if(fs.existsSync('./images-ios')){
        deleteFolderRecursive('./images-ios');
        console.log('remove folder ' + './images-ios');
    }

    // if local dropbox
    if(fs.existsSync(path.homedir() + DROPBOX_PATH)){
        console.log('move from local dropbox');
        copyRecursiveSync(path.homedir() + DROPBOX_PATH, IMG_PATH_SRC);
        //buildImages();
    }
    else {
        console.log('local dropbox not found');
        return false;
    }

    return true;

};

var downloadDropboxFile = function () {

    // remove .zip file in download folder
    if(fs.existsSync(FILE_DOWNLOAD_ZIP)){
        fs.unlinkSync(FILE_DOWNLOAD_ZIP);
        console.log('remove folder ' + FILE_DOWNLOAD_ZIP);
    }

    // open url and download
    open(DROPBOX_DOWNLOAD_URL, function (err) {
            if (err) throw err;
        console.log('----------------------------');
        console.log('    download from server    ');
        console.log('----------------------------');
    });

};

var recurseTime = function (check, callback){

    this.check = check;
    this.callback = callback;

    this.timeout = 30000;
    this.currentTime = 0;
    this.delay = 1000;

    this.recurse = function () {
        var that = this;
        if(that.currentTime < that.timeout){
            that.currentTime += that.delay;
            setTimeout(function(){
                if(that.check()){
                    that.callback();
                }
                else {
                    that.recurse();
                }
            }, that.delay);
        }
    };

    this.init = function () {
        this.recurse();
    }

   return this;
};


var unzipRecurse = new recurseTime(
    function(){
        if(!fs.existsSync(FILE_DOWNLOAD_ZIP)){
            console.log(FILE_DOWNLOAD_ZIP + 'not found !');
            return false
        }
        else {
            var stats = fs.statSync(FILE_DOWNLOAD_ZIP);
            if(stats.size !== 0){
                return true;
            }
            else {
                console.log('downloading...');
                return false;
            }
        }
    }, function () {
        unzip();
        buildImagesRecurse.init();
    }
);


var buildImagesRecurse = new recurseTime(
    function () {
        if(!fs.existsSync(IMG_PATH_SRC)){
            console.log(IMG_PATH_SRC + ' not found !');
            return false;
        }
        else {
            return true;
        }
    }, function(){
        buildImages();
    }
);



var unzip = function (build) {

    // remove images-ios folder in NodeJS projet
    if(fs.existsSync(IMG_PATH_SRC)){
        deleteFolderRecursive('./images-ios');
        console.log('remove folder ' + './images-ios');
    }

    // unzip and move
    exec('unzip ' + FILE_DOWNLOAD_ZIP + ' -d ./images-ios', puts);

};


var buildImages = function () {
    if(fs.existsSync(IMG_PATH_OUTPUT)){
        deleteFolderRecursive(IMG_PATH_OUTPUT);
        console.log('remove folder ' + IMG_PATH_OUTPUT);
    }

    // create output folder
    fs.mkdirSync(IMG_PATH_OUTPUT);
    console.log('create folder ' + IMG_PATH_OUTPUT);

    readFolder();
};


var readFolder = function (folder) {

    var _folder = folder || '';

    fs.readdir(IMG_PATH_SRC + _folder, function(error, files){
        if(error){
            console.log('---------------------');
            console.log('    ' + error + '    ');
            console.log('---------------------');
            return false;
        }

        // for each files
        for(var i = 0; i < files.length; i += 1){
            var file = files[i];

            if(['.DS_Store'].indexOf(file) !== -1) continue;
            if(['__MACOSX'].indexOf(file) !== -1) continue;
            
            if(['animations'].indexOf(file) !== -1){
                //fs.mkdirSync(IMG_PATH_OUTPUT + _folder + file);
                copyRecursiveSync(IMG_PATH_SRC + _folder + file, IMG_PATH_OUTPUT + _folder + file);
                continue;
            }

            _folder = (_folder !== '' && _folder.substr(_folder.length - 1) !== '/') ? _folder + '/' : _folder;

            var stats = fs.statSync(IMG_PATH_SRC + _folder + file);
            if(stats.isDirectory()){
                fs.mkdirSync(IMG_PATH_OUTPUT + _folder + file);
                readFolder(_folder + file);
            }
            else {
                createImg(_folder, file);
            }
        }

        console.log('-------------------------------');
        console.log('    end read folder ' + _folder);
        console.log('-------------------------------');

    });
};

String.prototype.clean = function () {
    var string = this;
    var find = ['"',"'",' ','’',';',',',':','à','á','â','ã','ä','ç','è','é','ê','ë','ì','í','î','ï','ñ','ò','ó','ô','õ','ö','ù','ú','û','ü','ý','ÿ','À','Á','Â','Ã','Ä','Ç','È','É','Ê','Ë','Ì','Í','Î','Ï','Ñ','Ò','Ó','Ô','Õ','Ö','Ù','Ú','Û','Ü','Ý'];
    var replace = ['','','-','','','','','a','a','a','a','a','c','e','e','e','e','i','i','i','i','n','o','o','o','o','o','u','u','u','u','y','y','A','A','A','A','A','C','E','E','E','E','I','I','I','I','N','O','O','O','O','O','U','U','U','U','Y'];
    var replaceString = string;
    var regex; 
    for (var i = 0; i < find.length; i++) {
        regex = new RegExp(find[i], "g");
        replaceString = replaceString.replace(regex, replace[i]);
    }
    replaceString = replaceString.toLowerCase();
    replaceString = replaceString.trim();
 
    return replaceString;
};

var createImg = function (folder, img) {
    var _folder = folder;
    var _img = img;
    var imgClean = _img.clean();

    var imageSetFolder = IMG_PATH_OUTPUT + _folder +  imgClean.replace('@2x', '').replace('.png', '') + IMG_FOLDER_SUFFIX;
    var imgOld = IMG_PATH_SRC + folder + _img;

    // if folder not exist
    if(!fs.existsSync(imageSetFolder)){
        fs.mkdirSync(imageSetFolder);
    }

    fs.writeFileSync(imageSetFolder + '/' + imgClean, fs.readFileSync(imgOld));
    console.log('move', imgOld);
    setContentsJSON(imageSetFolder, imgClean);
};


var setContentsJSON = function (imgPath, imgName) {

    var contentJSON;
    var JSONFilePath = imgPath + '/Contents.json';

    if(fs.existsSync(JSONFilePath)){
        var data = fs.readFileSync(JSONFilePath);
        contentJSON = JSON.parse(data);
    }
    else {
        contentJSON = {
            'images' : [
                {
                    'idiom' : 'universal',
                    'scale' : '1x'
                },
                {
                    'idiom' : 'universal',
                    'scale' : '2x'
                },
                {
                    'idiom' : 'universal',
                    'scale' : '3x'
                }
            ],
            'info' : {
                'version' : 1,
                'author' : 'xcode'
            }
        };
    }
    

    // //if not @2x
    // if(imgName.indexOf('@2x') === -1){
    //     contentJSON.images[0].filename = imgName;
    // }
    // // if @2x
    // else {
    //     contentJSON.images[1].filename = imgName;
    // }

    // if @2x
    if(imgName.indexOf('@2x') !== -1){
        contentJSON.images[1].filename = imgName;

    }
    // if @2x
    else if (imgName.indexOf('@3x') !== -1) {
        contentJSON.images[2].filename = imgName;
    }
    else {
        contentJSON.images[0].filename = imgName;
    }

    fs.writeFileSync(JSONFilePath, JSON.stringify(contentJSON));

};


var deleteFolderRecursive = function(path) {
    var files = [];
    if( fs.existsSync(path) ) {
        files = fs.readdirSync(path);
        files.forEach(function(file,index){
            var curPath = path + "/" + file;
            if(fs.lstatSync(curPath).isDirectory()) { // recurse
                deleteFolderRecursive(curPath);
            } else { // delete file
                fs.unlinkSync(curPath);
            }
        });
        fs.rmdirSync(path);
    }
};


var copyRecursiveSync = function(src, dest) {
  var exists = fs.existsSync(src);
  var stats = exists && fs.statSync(src);
  var isDirectory = exists && stats.isDirectory();
  if (exists && isDirectory) {
    fs.mkdirSync(dest);
    fs.readdirSync(src).forEach(function(childItemName) {
      copyRecursiveSync(path.join(src, childItemName),
                        path.join(dest, childItemName));
    });
  } else {
    fs.linkSync(src, dest);
  }
};


init();
